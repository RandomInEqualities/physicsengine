
#include <stdexcept>
#include "Entity.hpp"
#include "Physics.hpp"
#include "Geometry.hpp"
#include "Math/Vec3.hpp"
#include "Math/Mat4.hpp"

#include <iostream>
#include "Math/Debug.hpp"


Entity::Entity()
{

}


Entity::Entity(const IndexMesh& mesh) : 
    elasticCoefficient(0.2f),
    boundingRadius(0.0f),
    mass(1.0f),
    position(0.0f, 0.0f, 0.0f),
    velocityLinear(0.0f, 0.0f, 0.0f),
    velocityAngular(0.0f, 0.0f, 0.0f),
    state(State::Dynamical),
    mMesh(mesh)
{
    rotation.setupIdentity();
    boundingRadius = computeBoundingRadius(mesh);
}


Entity::Entity(const Entity& entity, const IndexMesh& newMesh) :
    elasticCoefficient(entity.elasticCoefficient),
    boundingRadius(entity.boundingRadius),
    mass(entity.mass),
    position(entity.position),
    velocityLinear(entity.velocityLinear),
    velocityAngular(entity.velocityAngular),
    state(entity.state),
    rotation(entity.rotation),
    mMesh(newMesh)
{
    boundingRadius = computeBoundingRadius(newMesh);
}


const IndexMesh& Entity::getGeometry() const
{
    return mMesh;
}


Mat4 Entity::getModelMatrix() const
{
    return Mat4::translate(-position) * rotation.toMat4();
}


Vec3 Entity::getInertia() const
{
    return Vec3(2.0f/5.0f*mass*boundingRadius*boundingRadius, 2.0f/5.0f*mass*boundingRadius*boundingRadius, 2.0f/5.0f*mass*boundingRadius*boundingRadius);
}


void Entity::setMaterial(const Material& material)
{
    for (Material& current : mMesh.materials) {
        current = material;
    }
}


void Entity::setMaterial(const std::vector<Material>& materials)
{
    if (materials.size() < mMesh.materials.size()) {
        throw std::out_of_range("Entity: material vector has too few materials.");
    }
    for (std::size_t index = 0; index < mMesh.materials.size(); index++) {
        mMesh.materials[index] = materials[index];
    }
}


float Entity::computeBoundingRadius(const IndexMesh& mesh)
{
    // Compute the bounding radius of the entity.
    float radius = 0.0f;
    for(const IndexTriangle& tri : mesh.triangles) {

        float v0length = mesh.vertices[tri.v0].length();
        float v1length = mesh.vertices[tri.v1].length();
        float v2length = mesh.vertices[tri.v2].length();

        if (v0length > radius) {
            radius = v0length;
        }
        if (v1length > radius) {
            radius = v1length;
        }
        if (v2length > radius) {
            radius = v2length;
        }

    }
    return radius;
}
