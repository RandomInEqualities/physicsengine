#include <vector>
#include <iostream>
#include "PhysicsTriangles.hpp"
#include "Physics.hpp"
#include "Entity.hpp"
#include "Terrain.hpp"
#include "Math/Math.hpp"
#include "Math/Debug.hpp"


PhysicsTriangles::PhysicsTriangles() :
    gravity(0,0,0)
{

}


void PhysicsTriangles::setGravity(Vec3 gravity) {
    this->gravity = gravity;
}


void PhysicsTriangles::update(float deltaTime, Scene& scene)
{
    // TODO
}


bool PhysicsTriangles::collide(const Entity& entityA, const Entity& entityB)
{
    // The spheres collide if the distance between the spheres is smaller than
    // the sum of their radia.
    if ((entityA.position - entityB.position).length() < entityA.boundingRadius + entityB.boundingRadius) {
        return true;
    }
    return false;
}


bool PhysicsTriangles::collide(const Entity& entity, Terrain& terrain)
{
    // The spheres can collide if the distance between the spheres is smaller than
    // the sum of their radia.
    if (Math::abs(entity.position.z - terrain.getHeight(entity.position.x, entity.position.y)) < entity.boundingRadius)  {
        return true;
    }
    return false;
}



void PhysicsTriangles::impulseInelastic(Entity& ent1, Entity& ent2)
{
    // TODO.
}


void PhysicsTriangles::impulseInelastic(Entity& ent, Terrain& terrain)
{
    // TODO.
}