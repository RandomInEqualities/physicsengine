////////////////////////////////////////////////////////////////////////
//
// ObjModel.cpp
//
////////////////////////////////////////////////////////////////////////



#include <vector>
#include <algorithm>
#include <string>
#include <cstddef>
#include <stdexcept>
#include <fstream>
#include <utility>
#include <ios>
#include <cctype>
#include <sstream>

using std::size_t;

#include "WavefrontObj.hpp"
#include "../Math/Vec2.hpp"
#include "../Math/Vec3.hpp"



////////////////////////////////////////////////////////////////////////
//
// Private function declarations
//
////////////////////////////////////////////////////////////////////////



namespace obj
{

    // Convert a string to a float or int. Throws logic_error if it can not convert the whole string.
    float stringToFloat(const std::string& str);
    int stringToInt(const std::string& str);

    // Try to read a vertex string into a face structure. Throws logic_error on failure.
    void stringToVertex(std::string str, IndexFace& face);

    // The face needs to be balanced such that there are either a normal for each vertex or
    // zero normals. The same goes for the texture positions, zero or as many as there are vertices.
    // Throws logic_error if face is unbalanced.
    void checkFaceBalance(const IndexFace& face);

    // Function for easily throwing a runtime_error to signal a syntax error in the parsing of a file.
    void throwRuntimeError(const std::string& filename, int line);

    // Convert a string to lower case.
    std::string toLower(const std::string& str);

    // Split a string into words.
    std::vector<std::string> splitIntoWords(const std::string& str);

}



////////////////////////////////////////////////////////////////////////
//
// Material methods
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
obj::Material::Material()
{
    reset();
}


////////////////////////////////////////////////////////////////////////
void obj::Material::reset()
{
    name.clear();
    ambient = Vec3(0.0, 0.0, 0.0);
    diffuse = Vec3(0.0, 0.0, 0.0);
    specular = Vec3(0.0, 0.0, 0.0);
    shininess = 1.0;
    transparency = 1.0;
    illumination = 0;
    ambientFilename.clear();
    diffuseFilename.clear();
    specularFilename.clear();
}



////////////////////////////////////////////////////////////////////////
//
// Model methods
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
void obj::Model::readFromFile(const std::string& filename)
{
    name.clear();
    faces.clear();
    vertices.clear();
    normals.clear();
    textureCoordinates.clear();
    groups.clear();
    materials.clear();

    std::ifstream file;
    file.open(filename);
    if (!file) {
        throw std::runtime_error(filename + " unable to open.");
    }
    file.exceptions(std::ifstream::badbit);

    // We use a subset of the object file format, see ObjModel.hpp for more info or
    // http://paulbourke.net/dataformats/obj/ for a full object file specification.

    // The currently active group and material. Initially default values.
    std::size_t group = 0;
    std::size_t material = 0;
    groups.push_back("unspecified");
    materials.push_back(Material());

    int lineNumber = 0;
    std::string line;
    while (std::getline(file, line)) {

        lineNumber++;

        std::vector<std::string> words = splitIntoWords(line);
        if (words.empty()) {
            continue;
        }
        words[0] = toLower(words[0]);

        if (words[0] == "v") {

            if (words.size() != 4 && words.size() != 5) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                float x = stringToFloat(words[1]);
                float y = stringToFloat(words[2]);
                float z = stringToFloat(words[3]);
                vertices.emplace_back(x, y, z);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "vn") {

            if (words.size() != 4) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                float x = stringToFloat(words[1]);
                float y = stringToFloat(words[2]);
                float z = stringToFloat(words[3]);
                normals.emplace_back(x, y, z);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "vt") {

            if (words.size() != 3 && words.size() != 4) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                float x = stringToFloat(words[1]);
                float y = stringToFloat(words[2]);
                textureCoordinates.emplace_back(x, y);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "f") {

            if (words.size() < 4) {
                throwRuntimeError(filename, lineNumber);
            }
            IndexFace indexFace;
            try {
                for (size_t index = 1; index < words.size(); index++) {
                    stringToVertex(words[index], indexFace);
                }
                checkFaceBalance(indexFace);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }
            indexFace.group = group;
            indexFace.material = material;
            faces.emplace_back(std::move(indexFace));

        }
        else if (words[0] == "mtllib") {
            for (size_t index = 1; index < words.size(); index++) {
                readMaterialFile(words[index]);
            }
        }
        else if (words[0] == "usemtl") {
            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                material = getMaterialIndex(words[1]);
            }
            catch (std::out_of_range) {
                throwRuntimeError(filename, lineNumber);
            }
        }
        else if (words[0] == "g") {
            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            group = getGroupIndex(words[1]);
        }
        else if (words[0] == "o") {
            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            name = words[1];
        }
    }

    // Check that each faces has valid vertices, normals, texture positions, material and group.
    for (const IndexFace& face : faces) {

        for (size_t index : face.vertices) {
            if (vertices.size() <= index) {
                std::string indexStr = std::to_string(index);
                throw std::runtime_error(filename + " invalid face vertex index " + indexStr + ".");
            }
        }

        for (size_t index : face.normals) {
            if (normals.size() <= index) {
                std::string indexStr = std::to_string(index);
                throw std::runtime_error(filename + " invalid face normal index " + indexStr + ".");
            }
        }

        for (size_t index : face.textureCoordinates) {
            if (textureCoordinates.size() <= index) {
                std::string indexStr = std::to_string(index);
                throw std::runtime_error(filename + " invalid face texture index " + indexStr + ".");
            }
        }

        if (materials.size() <= face.material) {
            std::string indexStr = std::to_string(face.material);
            throw std::runtime_error(filename + " invalid material index " + indexStr + ".");
        }

        if (groups.size() <= face.group) {
            std::string indexStr = std::to_string(face.group);
            throw std::runtime_error(filename + " invalid group index " + indexStr + ".");
        }

    }
}


////////////////////////////////////////////////////////////////////////
size_t obj::Model::getMaterialIndex(const std::string& materialName) const
{
    for (size_t index = 0; index < materials.size(); index++) {
        if (materialName == materials[index].name) {
            return static_cast<int>(index);
        }
    }
    throw std::out_of_range(name + " unable to find material " + materialName + ".");
}


////////////////////////////////////////////////////////////////////////
size_t obj::Model::getGroupIndex(const std::string& group)
{
    for (size_t index = 0; index < groups.size(); index++) {
        if (group == groups[index]) {
            return static_cast<int>(index);
        }
    }
    groups.push_back(group);
    return static_cast<int>(groups.size() - 1);
}


////////////////////////////////////////////////////////////////////////
void obj::Model::addMaterial(const obj::Material& material)
{
    if (material.name.empty()) {
        return;
    }

    // check that we do not have duplicates.
    for (Material& existing : materials) {
        if (material.name == existing.name) {
            existing = material;
            return;
        }
    }

    materials.push_back(material);
}


////////////////////////////////////////////////////////////////////////
void obj::Model::readMaterialFile(const std::string& filename)
{
    std::ifstream file(filename);
    if (!file) {
        throw std::runtime_error(filename + " unable to open.");
    }
    file.exceptions(std::ifstream::badbit);

    // The mtl file format has a fairly simple specification. It is based on the Phong light
    // reflection model, but can easily be extended or used for other models. An mtl file
    // specification can be found at http://paulbourke.net/dataformats/mtl/.

    // The material that we are currently loading.
    Material material;

    std::string line;
    int lineNumber = 0;
    while (std::getline(file, line)) {
        lineNumber++;

        std::vector<std::string> words = splitIntoWords(line);
        if (words.empty()) {
            continue;
        }
        words[0] = toLower(words[0]);

        if (words[0] == "newmtl") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            addMaterial(material);
            material.reset();
            material.name = words[1];

        }
        else if (words[0] == "ka" || words[0] == "kd" || words[0] == "ks") {

            if (words.size() != 4) {
                throwRuntimeError(filename, lineNumber);
            }

            Vec3 colour;
            try {
                colour.x = stringToFloat(words[1]);
                colour.y = stringToFloat(words[2]);
                colour.z = stringToFloat(words[3]);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

            if (words[0] == "ka") {
                material.ambient = colour;
            }
            else if (words[0] == "kd") {
                material.diffuse = colour;
            }
            else if (words[0] == "ks") {
                material.specular = colour;
            }

        }
        else if (words[0] == "ns") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                material.shininess = stringToFloat(words[1]);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "tr" || words[0] == "d") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                material.transparency = stringToFloat(words[1]);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "illum") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            try {
                material.illumination = stringToInt(words[1]);
            }
            catch (std::logic_error) {
                throwRuntimeError(filename, lineNumber);
            }

        }
        else if (words[0] == "map_ka") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            material.ambientFilename = words[1];

        }
        else if (words[0] == "map_kd") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            material.diffuseFilename = words[1];

        }
        else if (words[0] == "map_ks") {

            if (words.size() != 2) {
                throwRuntimeError(filename, lineNumber);
            }
            material.specularFilename = words[1];

        }
    }
    addMaterial(material);
}



////////////////////////////////////////////////////////////////////////
//
// Private functions
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
float obj::stringToFloat(const std::string& str)
{
    size_t end = 0;
    float result = std::stof(str, &end);
    if (end != str.size()) {
        throw std::invalid_argument("StringToFloat: unable to convert.");
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
int obj::stringToInt(const std::string& str)
{
    size_t end = 0;
    int result = std::stoi(str, &end);
    if (end != str.size()) {
        throw std::invalid_argument("StringToInt: unable to convert.");
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
void obj::stringToVertex(std::string str, obj::IndexFace& face)
{
    // An face string can have four forms v, v/vt, v/vt/vn and v//vn, where v = vertex,
    // vn = normal and vt = texture position.
    // ... read the vertex index.
    size_t readEnd = 0;
    face.vertices.emplace_back(std::stoul(str, &readEnd) - 1);
    if (readEnd == str.size()) {
        return;
    }
    if (str.at(readEnd) != '/') {
        throw std::invalid_argument("Bad vertex.");
    }

    // ... read the texture position index.
    if (str.at(readEnd + 1) != '/') {
        str.erase(0, readEnd + 1);
        face.textureCoordinates.emplace_back(std::stoul(str, &readEnd) - 1);
        if (readEnd == str.size()) {
            return;
        }
        if (str.at(readEnd) != '/') {
            throw std::invalid_argument("Bad vertex.");
        }
        str.erase(0, readEnd + 1);
    }
    else {
        str.erase(0, readEnd + 2);
    }

    // ... read the normal vector index.
    face.normals.emplace_back(std::stoul(str, &readEnd) - 1);
    if (readEnd != str.size()) {
        throw std::invalid_argument("Bad vertex.");
    }
}


////////////////////////////////////////////////////////////////////////
void obj::checkFaceBalance(const obj::IndexFace& face)
{
    size_t vSize = face.vertices.size();
    size_t vnSize = face.normals.size();
    size_t vtSize = face.textureCoordinates.size();

    if (vnSize != 0 && vnSize != vSize) {
        throw std::logic_error("Unbalanced face");
    }
    if (vtSize != 0 && vtSize != vSize) {
        throw std::logic_error("Unbalanced face");
    }

    return;
}


////////////////////////////////////////////////////////////////////////
void obj::throwRuntimeError(const std::string& filename, int line)
{
    throw std::runtime_error(filename + " syntax error at line " + std::to_string(line));
}


////////////////////////////////////////////////////////////////////////
std::string obj::toLower(const std::string& str)
{
    std::string result;
    for (char ch : str) {
        result.push_back(std::tolower(ch));
    }
    return result;
}


////////////////////////////////////////////////////////////////////////
std::vector<std::string> obj::splitIntoWords(const std::string& str)
{
    std::vector<std::string> words;
    std::istringstream stream(str);
    std::string word;
    while (stream >> word) {
        if (!word.empty()) {
            words.push_back(word);
        }
    }
    return words;
}