////////////////////////////////////////////////////////////////////////
//
// WavefrontObj.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef WAVEFRONT_OBJ_HPP_INCLUDED
#define WAVEFRONT_OBJ_HPP_INCLUDED

#include <vector>
#include <string>
#include <cstddef>
#include "../Math/Vec2.hpp"
#include "../Math/Vec3.hpp"


/*
Object files are used to hold geometry in ASCII format. They have the obj extension.
Material template library files (mtl extension) are used to hold textures and Phong light
model parameters.

We provide a subset of the obj file specification, see http://paulbourke.net/dataformats/obj/
for a full specification. Right now we look for vertices (v), faces (f), texture coordinates
(vt), normals (vn), materials (usemtl, mtllib), group names (g) and object names (o). When
mtllib is detected we load the the material file. In the material file we look for
transparency (tr, d), ambient (ka, map_ka), diffuse (kd, map_kd), specular (ks, map_ks),
specular shine (ns), illumination mode (illum) and of course new materials (newmtl).

Model holds the data of a complete object file.
Material holds the data of a single material.
IndexFace holds the data for a single face in the model.
*/
namespace obj
{

class Material
{
public:

    // Constructor sets default material parameters.
    Material();

    // Reset the material to the default parameters.
    void reset();

    // The material name. Faces reference this name.
    std::string name;

    // Material parameters. The ambient, diffuse, specular and transparency are between 0.0
    // and 1.0. The shininess is positive. The illumination specifies the desired illumination 
    // mode.
    Vec3 ambient;
    Vec3 diffuse;
    Vec3 specular;
    float shininess;
    float transparency;
    int illumination;

    // Optional textures. We hold a filename to the textures.
    std::string ambientFilename;
    std::string diffuseFilename;
    std::string specularFilename;
};

class IndexFace
{
public:

    // Index locations of the face vertices.
    std::vector<std::size_t> vertices;

    // Group. If the value is 0 the face has a default group.
    std::size_t group;

    // Material. If the value is 0 the face has a default material.
    std::size_t material;

    // Optional index locations of texture positions and normals. The sizes are either zero 
    // or vertices.size().
    std::vector<std::size_t> textureCoordinates;
    std::vector<std::size_t> normals;

};

class Model
{
public:

    // The object name.
    std::string name;

    // The geometries in the object. It holds indices into the below vertex, normal, texture
    // coordinate, group and material arrays.
    std::vector<IndexFace> faces;

    // The vertex positions.
    std::vector<Vec3> vertices;

    // The vertex normals.
    std::vector<Vec3> normals;

    // The texture positions. The x and y values are between 0.0 and 1.0.
    std::vector<Vec2> textureCoordinates;

    // The groups.
    std::vector<std::string> groups;

    // The materials.
    std::vector<Material> materials;

    // Load the model from an obj file. Throws runtime_error on failure.
    void readFromFile(const std::string& filename);

private:

    // Get material index from a material name. Throws out_of_range on failure.
    std::size_t getMaterialIndex(const std::string& materialName) const;

    // Get an index for a group. Inserts the group if it was not found.
    std::size_t getGroupIndex(const std::string& group);

    // Add a new material to the model. Overwrites any stored material with the same name.
    void addMaterial(const Material& material);

    // Try to read and load a material file. Throws runtime_error on failure.
    void readMaterialFile(const std::string& filename);

};

}


#endif // WAVEFRONT_OBJ_HPP_INCLUDED
