
#include <map>
#include <cstddef>
#include <stdexcept>
#include <cassert>
#include <GL/glew.h>

#include "PhongShader.hpp"
#include "Shader.hpp"
#include "Lights.hpp"
#include "Geometry.hpp"
#include "OpenGL.hpp"
#include "Math/Mat4.hpp"


PhongShader::PhongShader() : 
    MAX_NUMBER_OF_LIGHTS(5),
    positionAttribute(-1),
    normalAttribute(-1),
    modelMatrixUniform(-1),
    viewMatrixUniform(-1),
    projectionMatrixUniform(-1),
    lightAmountUniform(-1),
    lightPositionUniform(-1),
    lightDiffuseUniform(-1),
    lightSpecularUniform(-1),
    materialAmbientUniform(-1),
    materialDiffuseUniform(-1),
    materialSpecularUniform(-1),
    materialShininessUniform(-1)
{

}


void PhongShader::compile() 
{
    glCheckInitialization();

    if (glIsProgram(mShaderProgram) == GL_TRUE) {
        glDeleteProgram(mShaderProgram);
    }

    // Load and compile the shader program.
    mShaderProgram = loadShaderFromFile("Shaders/phong.vert", "Shaders/phong.frag");
    bind();

    // Find the shader parameters.
    positionAttribute = findAttributeLocation(mShaderProgram, "position");
    normalAttribute = findAttributeLocation(mShaderProgram, "normal");

    modelMatrixUniform = findUniformLocation(mShaderProgram, "modelMatrix");
    viewMatrixUniform = findUniformLocation(mShaderProgram, "viewMatrix");
    projectionMatrixUniform = findUniformLocation(mShaderProgram, "clipMatrix");
    lightAmountUniform = findUniformLocation(mShaderProgram, "numberOfLights");
    lightPositionUniform = findUniformLocation(mShaderProgram, "lightPosition");
    lightDiffuseUniform = findUniformLocation(mShaderProgram, "lightDiffuse");
    lightSpecularUniform = findUniformLocation(mShaderProgram, "lightSpecular");
    materialAmbientUniform = findUniformLocation(mShaderProgram, "materialAmbient");
    materialDiffuseUniform = findUniformLocation(mShaderProgram, "materialDiffuse");
    materialSpecularUniform = findUniformLocation(mShaderProgram, "materialSpecular");
    materialShininessUniform = findUniformLocation(mShaderProgram, "materialShininess");

    glCheckError();
}


VertexArray PhongShader::createVertexArray(const VertexBufferModel& model)
{
    assert(ready());
    bind();

    VertexArray vao;
    glGenVertexArrays(1, &vao.location);
    glBindVertexArray(vao.location);

    // Vertex positions.
    if (!model.haveVertices()) {
        throw std::runtime_error("PhongShader: VertexBufferModel does not have vertex data.");
    }

    glBindBuffer(GL_ARRAY_BUFFER, model.vertices.location);
    glVertexAttribPointer(
        positionAttribute, 
        model.vertices.size, 
        model.vertices.type, 
        GL_FALSE, 
        model.vertices.stride, 
        model.vertices.offset
        );
    glEnableVertexAttribArray(positionAttribute);

    // Vertex normals.
    if (!model.haveNormals()) {
        throw std::runtime_error("PhongShader: VertexBufferModel does not have vertex normal data.");
    }

    glBindBuffer(GL_ARRAY_BUFFER, model.normals.location);
    glVertexAttribPointer(
        normalAttribute, 
        model.normals.size, 
        model.normals.type, 
        GL_FALSE, 
        model.normals.stride, 
        model.normals.offset
        );
    glEnableVertexAttribArray(normalAttribute);

    glCheckError();

    vao.shader = Shader::Type::Phong;
    vao.drawing = model.drawing;
    return vao;
}


void PhongShader::setProjectionMatrix(const Mat4& matrix)
{
    if (projectionMatrixUniform != -1) {
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, matrix.toArray());
    }
}


void PhongShader::setModelMatrix(const Mat4& matrix)
{
    if (modelMatrixUniform != -1) {
        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, matrix.toArray());
    }
}


void PhongShader::setViewMatrix(const Mat4& matrix)
{
    if (viewMatrixUniform != -1) {
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, matrix.toArray());
    }
}


void PhongShader::setMaterial(const Material& material)
{
    if (materialAmbientUniform != -1) {
        glUniform3f(materialAmbientUniform, material.ambient.x, material.ambient.y, material.ambient.z);
    }
    if (materialDiffuseUniform != -1) {
        glUniform3f(materialDiffuseUniform, material.diffuse.x, material.diffuse.y, material.diffuse.z);
    }
    if (materialSpecularUniform != -1) {
        glUniform3f(materialSpecularUniform, material.specular.x, material.specular.y, material.specular.z);
    }
    if (materialShininessUniform != -1) {
        glUniform1f(materialShininessUniform, material.shininess);
    }
}


void PhongShader::setLights(const std::map<int, PointLight>& pointLights, 
                            const std::map<int, DirectionalLight>& directionalLights) 
{
    assert(ready());

    if (pointLights.size() + directionalLights.size() > MAX_NUMBER_OF_LIGHTS) {
        throw std::out_of_range("PhongShader: there are too many lights");
    }

    // Construct arrays with light data that can be sent to opengl.
    std::vector<float> positions;
    std::vector<float> diffuse;
    std::vector<float> specular;
    for (const auto& light : pointLights) {
        positions.push_back(light.second.position.x);
        positions.push_back(light.second.position.y);
        positions.push_back(light.second.position.z);
        positions.push_back(1.0f);
        addToArray(light.second.diffuse, diffuse);
        addToArray(light.second.specular, specular);
    }
    for (const auto& light : directionalLights) {
        positions.push_back(light.second.direction.x);
        positions.push_back(light.second.direction.y);
        positions.push_back(light.second.direction.z);
        positions.push_back(0.0f);
        addToArray(light.second.diffuse, diffuse);
        addToArray(light.second.specular, specular);
    }

    // Send the arrays to the proper uniform parameters.
    GLsizei count = static_cast<GLsizei>(pointLights.size() + directionalLights.size());
    if (lightAmountUniform != -1) {
        glUniform1i(lightAmountUniform, count);
    }
    if (lightPositionUniform != -1) {
        glUniform4fv(lightPositionUniform, count, positions.data());
    }
    if (lightDiffuseUniform != -1) {
        glUniform3fv(lightDiffuseUniform, count, diffuse.data());
    }
    if (lightSpecularUniform != -1) {
        glUniform3fv(lightSpecularUniform, count, specular.data());
    }
    glCheckError();
}