#ifndef PHONG_SHADER_HPP_INCLUDED
#define PHONG_SHADER_HPP_INCLUDED

#include <map>
#include <cstddef>
#include <GL/glew.h>

#include "Shader.hpp"
#include "Lights.hpp"
class Mat4;
class Material;
class VertexBufferModel;
class VertexArray;


class PhongShader : public Shader
{
public:

    PhongShader();

    // Compile shader.
    void compile() override;

    // Create a vertex array object that links this shaders attributes to the
    // given vertex buffer.
    VertexArray createVertexArray(const VertexBufferModel& model) override;

    // Set the uniform matrix variables in the shader.
    void setModelMatrix(const Mat4& matrix);
    void setViewMatrix(const Mat4& matrix);
    void setProjectionMatrix(const Mat4& matrix);
    void setMaterial(const Material& material);
    void setLights(
        const std::map<int, PointLight>& pointLights, 
        const std::map<int, DirectionalLight>& directionalLights
    );

private:

    GLint positionAttribute;
    GLint normalAttribute;

    GLint modelMatrixUniform;
    GLint viewMatrixUniform;
    GLint projectionMatrixUniform;
    GLint lightAmountUniform;
    GLint lightPositionUniform;
    GLint lightDiffuseUniform;
    GLint lightSpecularUniform;
    GLint materialAmbientUniform;
    GLint materialDiffuseUniform;
    GLint materialSpecularUniform;
    GLint materialShininessUniform;

    std::size_t MAX_NUMBER_OF_LIGHTS;

};


#endif