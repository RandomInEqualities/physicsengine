#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <vector>
#include <map>

#include "OpenGL.hpp"
#include "Entity.hpp"
#include "Terrain.hpp"
#include "SkyBox.hpp"
#include "MeshLoader.hpp"
#include "PhongShader.hpp"
#include "WaterShader.hpp"
#include "SkyShader.hpp"
#include "Lights.hpp"
class Camera;


// The scene class holds the geometry for a scene. It handles loading and 
// drawing of the scene with OpenGL.
class Scene
{

public:

    Scene();
    ~Scene();

    // Initialize OpenGL so we can draw a scene.
    void initialize();

    // Draw the scene with OpenGL.
    void draw(const Camera& camera, float time);

    // Get scene objects. Terrain, skybox and entities are non-assignable.
    Terrain& getTerrain();
    SkyBox& getSkybox();
    Entity& getEntity(int index);
    PointLight& getPointLight(int index);
    DirectionalLight& getDirectionalLight(int index);

    // Set scene objects. Overwrites the current objects.
    void setTerrain(const Terrain& newTerrain);
    void setSkybox(const SkyBox& newSkybox);
    void updateEntity(const Entity& newEntity, int index);
    void updatePointLight(const PointLight& newLight, int index);
    void updateDirectionalLight(const DirectionalLight& newLight, int index);

    // Get amount of entities.
    bool hasTerrain() const;
    bool hasSkybox() const;
    std::size_t getEntityAmount() const;
    std::size_t getPointLightAmount() const;
    std::size_t getDirectionalLigtAmount() const;

    // Delete scene objects. This invalidates references returned by the get calls.
    void deleteTerrain();
    void deleteSkybox();
    void deleteEntity(int index);
    void deletePointLight(int index);
    void deleteDirectionalLight(int index);

    // Add object to the scene.
    int addEntity(const Entity& entity);
    int addPointLight(const PointLight& light);
    int addDirectionalLight(const DirectionalLight& light);

    // Add entity to the scene from a wavefront obj file. The object is cached so
    // multiple calls with the same filename is fast.
    int addEntityFromFile(const std::string& filename);

    // Get all scene entities. Do not change the vector! Only change individual elements.
    std::map<int, Entity>::iterator getEntityFirst();
    std::map<int, Entity>::iterator getEntityLast();

private:

    struct EntityResourceIndices
    {
        // This class matches an entity with a vertex array and a vertex buffer.
        // The values here are indices into the map where the vertex and vertex 
        // buffer are stored.
        EntityResourceIndices();
        EntityResourceIndices(int vertexArray, int vertexBuffer);
        int vertexArray;
        int vertexBuffer;
    };

    void drawTerrain(float time, const Mat4& view, const Mat4& projection);
    void drawSkybox(float time, const Mat4& view, const Mat4& projection);
    void drawEntities(float time, const Mat4& view, const Mat4& projection);

    // Delete OpenGL resources associated with an entity. Only deletes them if the
    // entity is the last one that uses them. Right now we loop through the drawing
    // arrays to check if this resource can be freed. We could reference count the 
    // VertexArray, VertexBuffer and VertexBufferModel classes, if this methods 
    // becomes too slow.
    void deleteResources(const EntityResourceIndices& entityResource);

    int mCurrentEntityId;
    int mCurrentVertexArrayId;
    int mCurrentVertexBufferId;
    std::map<int, Entity> mEntities;
    std::map<int, EntityResourceIndices> mEntityResourceIndices;
    std::map<int, VertexArray> mEntityVertexArrays;
    std::map<int, VertexBufferModel> mEntityVertexBuffers;

    Terrain mTerrain;
    VertexArray mTerrainVertexArray;
    VertexBufferModel mTerrainVertexBuffers;

    SkyBox mSkybox;
    VertexArray mSkyboxVertexArray;
    VertexBufferModel mSkyboxVertexBuffers;
    int mSunDirectionalLightId;

    int mCurrentPointLightId;
    int mCurrentDirectionalLightId;
    std::map<int, PointLight> mPointLights;
    std::map<int, DirectionalLight> mDirectionalLights;

    PhongShader mPhong;
    WaterShader mWater;
    SkyShader mSky;

    MeshLoader mFileLoader;

    // Cache with already loaded vertex buffers and vertex arrays. The key is the filename.
    std::map<std::string, int> mCachedVertexArrays;
    std::map<std::string, int> mCachedVertexBuffers;

};


#endif