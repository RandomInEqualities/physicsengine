
#include <cstddef>
#include <vector>
#include <stdexcept>

#include "Geometry.hpp"
#include "Math/Vec2.hpp"
#include "Math/Vec3.hpp"
#include "Shader.hpp"


Material::Material() : 
    ambient(1,1,1), 
    diffuse(1,1,1), 
    specular(1,1,1), 
    shininess(1)
{

}


Material::Material(Vec3 AMBIENT, Vec3 DIFFUSE, Vec3 SPECULAR, float SHININESS) :
    ambient(AMBIENT), 
    diffuse(DIFFUSE), 
    specular(SPECULAR), 
    shininess(SHININESS)
{

}


IndexTriangle::IndexTriangle() : 
    v0(0), v1(0), v2(0), v0normal(0), v1normal(0), v2normal(0), 
    v0texCoord(0), v1texCoord(0), v2texCoord(0), material(0)
{

}


IndexMesh::IndexMesh()
{

}


IndexMesh::IndexMesh(
    const std::vector<IndexTriangle>& TRIANGLES,
    const std::vector<Vec3>& VERTICES,
    const std::vector<Vec3>& NORMALS,
    const std::vector<Vec2>& TEXCOORDS,
    const std::vector<Material>& MATERIALS
) : 
    triangles(TRIANGLES),
    vertices(VERTICES),
    normals(NORMALS),
    texCoords(TEXCOORDS),
    materials(MATERIALS)
{
    if (haveOutOfRangeIndices()) {
        throw std::out_of_range("indices in IndexMesh is out of range");   
    }
}


bool IndexMesh::haveOutOfRangeIndices() const
{
    for (const IndexTriangle& index : triangles) {
        if (index.v0 >= vertices.size()) {
            return true;
        }
        if (index.v1 >= vertices.size()) {
            return true;
        }
        if (index.v2 >= vertices.size()) {
            return true;
        }
        if (index.v0normal >= normals.size()) {
            return true;
        }
        if (index.v1normal >= normals.size()) {
            return true;
        }
        if (index.v2normal >= normals.size()) {
            return true;
        }
        if (index.v0texCoord >= texCoords.size()) {
            return true;
        }
        if (index.v1texCoord >= texCoords.size()) {
            return true;
        }
        if (index.v2texCoord >= texCoords.size()) {
            return true;
        }
        if (index.material >= materials.size()) {
            return true;
        }
    }
    return false;
}


Vec3 computeNormal(Vec3 v0, Vec3 v1, Vec3 v2)
{
    return cross(v1 - v0, v2 - v0).normalize();
}


std::vector<Triangle> convertToSimpleMesh(const IndexMesh& mesh)
{
    std::vector<Triangle> result;
    for (const IndexTriangle& index : mesh.triangles) {
        Triangle triangle;
        triangle.v0 = mesh.vertices[index.v0];
        triangle.v1 = mesh.vertices[index.v1];
        triangle.v2 = mesh.vertices[index.v2];
        triangle.v0normal = mesh.normals[index.v0normal];
        triangle.v1normal = mesh.normals[index.v1normal];
        triangle.v2normal = mesh.normals[index.v2normal];
        triangle.v0texCoord = mesh.texCoords[index.v0texCoord];
        triangle.v1texCoord = mesh.texCoords[index.v1texCoord];
        triangle.v2texCoord = mesh.texCoords[index.v2texCoord];
        triangle.material = mesh.materials[index.material];
        result.emplace_back(triangle);
    }
    return result;
}


IndexMesh convertToIndexMesh(const std::vector<Triangle>& mesh)
{
    IndexMesh result;
    for (const Triangle& triangle : mesh) {
        result.vertices.emplace_back(triangle.v0);
        result.vertices.emplace_back(triangle.v1);
        result.vertices.emplace_back(triangle.v2);
        result.normals.emplace_back(triangle.v0normal);
        result.normals.emplace_back(triangle.v1normal);
        result.normals.emplace_back(triangle.v2normal);
        result.texCoords.emplace_back(triangle.v0texCoord);
        result.texCoords.emplace_back(triangle.v1texCoord);
        result.texCoords.emplace_back(triangle.v2texCoord);
        result.materials.emplace_back(triangle.material);

        IndexTriangle index;
        index.v0 = result.vertices.size() - 3;
        index.v1 = result.vertices.size() - 2;
        index.v2 = result.vertices.size() - 1;
        index.v0normal = result.normals.size() - 3;
        index.v1normal = result.normals.size() - 2;
        index.v2normal = result.normals.size() - 1;
        index.v0texCoord = result.texCoords.size() - 3;
        index.v1texCoord = result.texCoords.size() - 2;
        index.v2texCoord = result.texCoords.size() - 1;
        index.material = result.materials.size() - 1;

        result.triangles.emplace_back(index);
    }
    return result;
}


bool triangleLineIntersection(Vec3& position, Vec3 a, Vec3 b, Vec3 c, Vec3 lineP1, Vec3 lineP2)
{
    if (trianglePlaneIntersection(position, a, b, c, lineP1, lineP2)) {
        if (pointInTriangle(position, a, b, c)) {
            return true;
        }
    }
    return false;
}


bool trianglePlaneIntersection(Vec3& position, Vec3 a, Vec3 b, Vec3 c, Vec3 lineP1, Vec3 lineP2)
{
    Vec3 normal = computeNormal(a, b, c);
    Vec3 origin = lineP1;
    Vec3 direction = (lineP1 - lineP2).normalize();
    float dotNormalDir = dot(normal, direction);
    if (dotNormalDir < 0.01) {
        return false;
    }

    float t = (dot(normal, a) - dot(normal, origin)) / dotNormalDir;
    position = origin + t * direction;
    return true;
}


bool pointInTriangle(Vec3 position, Vec3 a, Vec3 b, Vec3 c)
{
    Vec3 v0 = b - a, v1 = c - a, v2 = position - a;
    float d00 = dot(v0, v0);
    float d01 = dot(v0, v1);
    float d11 = dot(v1, v1);
    float d20 = dot(v2, v0);
    float d21 = dot(v2, v1);
    float invDenom = 1.0f / (d00 * d11 - d01 * d01);
    float v = (d11 * d20 - d01 * d21) * invDenom;
    float w = (d00 * d21 - d01 * d20) * invDenom;
    float u = 1.0f - v - w;

    if (v < 0 || v > 1) {
        return false;
    }
    if (w < 0 || w > 1) {
        return false;
    }
    if (u < 0 || u > 1) {
        return false;
    }

    return true;
}


bool triangleTriangleIntersection(Vec3& position, Vec3 a0, Vec3 b0, Vec3 c0, Vec3 a1, Vec3 b1, Vec3 c1)
{
    Vec3 positionSum(0,0,0);
    int count = 0;

    Vec3 pos;
    if (triangleLineIntersection(pos, a0, b0, c0, a1, b1)) {
        positionSum += pos;
        count++;
    }
    if (triangleLineIntersection(pos, a0, b0, c0, a1, c1)) {
        positionSum += pos;
        count++;
    }
    if (triangleLineIntersection(pos, a0, b0, c0, b1, c1)) {
        positionSum += pos;
        count++;
    }
    if (triangleLineIntersection(pos, a1, b1, c1, a0, b0)) {
        positionSum += pos;
        count++;
    }
    if (triangleLineIntersection(pos, a1, b1, c1, a0, c0)) {
        positionSum += pos;
        count++;
    }
    if (triangleLineIntersection(pos, a1, b1, c1, b0, c0)) {
        positionSum += pos;
        count++;
    }

    if (count > 0) {
        position = positionSum/(float)count;
        return true;
    }
    return false;
}