#ifndef GAME_ENGINE_HPP_INCLUDED
#define GAME_ENGINE_HPP_INCLUDED

#include <vector>
#include <random>

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "Scene.hpp"
#include "Camera.hpp"
#include "PhysicsSphere.hpp"
#include "PhysicsTriangles.hpp"


class GameEngine 
{

public:

    GameEngine();
    ~GameEngine();

    void start();

private:

    void handleWindowEvents();

    void moveCamera(sf::Time delta);
    void updatePhysics(sf::Time delta);
    void render(sf::Time time);

    void mousePress(const sf::Event& event);
    void mouseMove(const sf::Event& event);
    void keyPress(const sf::Event& event);
    void keyRelease(const sf::Event& event);
    void windowResize(const sf::Event& event);

    void setupDefaultScene();
    void setupPhysicsTestScene();
    void setupReleaseScene1();
    void setupReleaseScene2();
    void setupReleaseScene3();
    void setupReleaseScene4();

    sf::Window window;
    sf::Vector2u windowSize;
    sf::Vector2i windowCenter;

    std::default_random_engine rd;

    // The game engine has a scene with objects, a camera that views the scene and
    // physics class(es) that compute the forces between objects.
    Scene scene;
    Camera camera;
    PhysicsSphere sphereCollision;
    PhysicsTriangles triangleCollision;

    float timeWarp;

};


#endif