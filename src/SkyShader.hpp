#ifndef SKYSHADER_HPP_INCLUDED
#define SKYSHADER_HPP_INCLUDED

#include <GL/glew.h>
#include "Shader.hpp"
class Mat4;
class VertexBufferModel;
class VertexArray;
class DirectionalLight;


class SkyShader : public Shader
{
public:

    SkyShader();

    // Compile the shader.
    void compile() override;

    // Create a vertex array object that links this shaders attributes to the
    // given vertex buffers.
    VertexArray createVertexArray(const VertexBufferModel& model) override;

    // Set the uniform variables in the shader.
    void setModelMatrix(const Mat4& matrix);
    void setViewMatrix(const Mat4& matrix);
    void setProjectionMatrix(const Mat4& matrix);
    void setTimeOfDay(float time);
    void setSun(const DirectionalLight& light);

private:

    GLint positionAttribute;
    GLint normalAttribute;

    GLint modelMatrixUniform;
    GLint viewMatrixUniform;
    GLint projectionMatrixUniform;
    GLint timeOfDayUniform;
    GLint sunColorUniform;
    GLint sunColorHorizonUniform;
    GLint sunDirectionUniform;

};

#endif