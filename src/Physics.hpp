#ifndef PHYSICS_HPP_INCLUDED
#define PHYSICS_HPP_INCLUDED

class Scene;
class Vec3;

/*
    Interface for all physics classes.
*/
class Physics 
{
public:

    ~Physics() {};

    virtual void setGravity(Vec3 gravity) = 0;
    virtual void update(float deltaTime, Scene& scene) = 0;

};


#endif // PHYSICS_HPP_INCLUDED