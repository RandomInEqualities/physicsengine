
#include <vector>
#include <map>
#include <iostream>
#include <cassert>
#include <GL/glew.h>

#include "Scene.hpp"
#include "OpenGL.hpp"
#include "Camera.hpp"
#include "Entity.hpp"
#include "Terrain.hpp"
#include "SkyBox.hpp"
#include "MeshLoader.hpp"
#include "PhongShader.hpp"
#include "SkyShader.hpp"
#include "Lights.hpp"
#include "Math/Mat4.hpp"
#include "Math/Debug.hpp"


Scene::EntityResourceIndices::EntityResourceIndices() :
    vertexArray(0),
    vertexBuffer(0)
{

}


Scene::EntityResourceIndices::EntityResourceIndices(int VERTEXARRAY, int VERTEXBUFFER) :
    vertexArray(VERTEXARRAY),
    vertexBuffer(VERTEXBUFFER)
{

}


Scene::Scene() :
    mCurrentEntityId(0),
    mCurrentVertexArrayId(0),
    mCurrentVertexBufferId(0),
    mCurrentPointLightId(0),
    mCurrentDirectionalLightId(0),
    mSunDirectionalLightId(0)
{

}


Scene::~Scene()
{
    // Find all vertex arrays.
    for (auto& vao : mEntityVertexArrays) {
        deleteVertexArray(vao.second);
    }
    deleteVertexArray(mTerrainVertexArray);
    deleteVertexArray(mSkyboxVertexArray);

    // Find all vertex buffers.
    for (auto& vbo : mEntityVertexBuffers) {
        deleteVertexBuffer(vbo.second);
    }
    deleteVertexBuffer(mTerrainVertexBuffers);
    deleteVertexBuffer(mSkyboxVertexBuffers);

    glCheckError();
}


void Scene::initialize()
{
    glCheckInitialization();

    mPhong.compile();
    mSky.compile();
    mWater.compile();

    // Enable depth testings.
    glEnable(GL_DEPTH_TEST);

    // Enable back face culling.
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    // Enable multisampling. Disable this if you want better performance.
    glEnable(GL_MULTISAMPLE);

    glCheckError();
}


void Scene::draw(const Camera& camera, float time)
{
    glClearColor(0.0f, 0.5f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Get the view port dimensions. 
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    int width = viewport[2];
    int height = viewport[3];

    // Get world to camera transformation matrix.
    Mat4 view = camera.getViewMatrix();

    // Get perspective projection transformation matrix.
    Mat4 projection = Mat4::perspective(
        camera.getFieldOfViewY(), 
        (float)width/(float)height,
        camera.getNearZ(),
        camera.getFarZ()
    );

    // Update sun position.
    updateDirectionalLight(mSkybox.getSunLight(time), mSunDirectionalLightId);

    drawSkybox(time, view, projection);
    drawTerrain(time, view, projection);
    drawEntities(time, view, projection);

    glCheckError();
}


Terrain& Scene::getTerrain()
{
    return mTerrain;
}


SkyBox& Scene::getSkybox()
{
    return mSkybox;
}


Entity& Scene::getEntity(int index)
{
    assert(mEntities.find(index) != mEntities.end());
    return mEntities[index];
}


PointLight& Scene::getPointLight(int index)
{
    assert(mPointLights.find(index) != mPointLights.end());
    return mPointLights[index];
}


DirectionalLight& Scene::getDirectionalLight(int index)
{
    assert(mDirectionalLights.find(index) != mDirectionalLights.end());
    return mDirectionalLights[index];
}


void Scene::setTerrain(const Terrain& terrain)
{
    const IndexMesh& mesh = terrain.getGeometry();

    deleteVertexArray(mTerrainVertexArray);
    deleteVertexBuffer(mTerrainVertexBuffers);

    mTerrain = terrain;
    mTerrainVertexBuffers = createVertexBuffer(mesh);
    mTerrainVertexArray = mPhong.createVertexArray(mTerrainVertexBuffers);

    glCheckError();
}


void Scene::setSkybox(const SkyBox& skybox)
{
    const IndexMesh& mesh = skybox.getGeometry();

    deleteVertexArray(mSkyboxVertexArray);
    deleteVertexBuffer(mSkyboxVertexBuffers);

    mSkybox = skybox;
    mSkyboxVertexBuffers = createVertexBuffer(mesh);
    mSkyboxVertexArray = mSky.createVertexArray(mSkyboxVertexBuffers);

    // Add sunlight from the sky shader.
    mSunDirectionalLightId = addDirectionalLight(skybox.getSunLight(0.0));

    glCheckError();
}


void Scene::updateEntity(const Entity& newEntity, int index)
{
    assert(mEntities.find(index) != mEntities.end());
    assert(mEntityResourceIndices.find(index) != mEntityResourceIndices.end());

    // Delete the current vertex array and buffer.
    deleteResources(mEntityResourceIndices[index]);

    // Update the entity with a new vertex array and buffer.
    mEntities[index] = newEntity;

    VertexBufferModel vbo = createVertexBuffer(newEntity.getGeometry());
    VertexArray vao = mPhong.createVertexArray(vbo);
    mEntityVertexBuffers[mCurrentVertexBufferId] = vbo;
    mEntityVertexArrays[mCurrentVertexArrayId] = vao;
    mEntityResourceIndices[index] = EntityResourceIndices(mCurrentVertexArrayId, mCurrentVertexBufferId);

    mCurrentVertexBufferId++;
    mCurrentVertexArrayId++;
}


void Scene::updatePointLight(const PointLight& newLight, int index)
{
    assert(mPointLights.find(index) != mPointLights.end());
    mPointLights[index] = newLight;
}


void Scene::updateDirectionalLight(const DirectionalLight& newLight, int index)
{
    assert(mDirectionalLights.find(index) != mDirectionalLights.end());
    mDirectionalLights[index] = newLight;
}


bool Scene::hasTerrain() const
{
    return mTerrainVertexArray.location != 0;
}


bool Scene::hasSkybox() const
{
    return mSkyboxVertexArray.location != 0;
}


std::size_t Scene::getEntityAmount() const
{
    return mEntities.size();
}


std::size_t Scene::getPointLightAmount() const
{
    return mPointLights.size();
}


std::size_t Scene::getDirectionalLigtAmount() const
{
    return mDirectionalLights.size();
}


void Scene::deleteTerrain()
{
    assert(mTerrainVertexArray.location != 0);
    deleteVertexArray(mTerrainVertexArray);
    deleteVertexBuffer(mTerrainVertexBuffers);
    mTerrain = Terrain();
    mTerrainVertexArray = VertexArray();
    mTerrainVertexBuffers = VertexBufferModel();
}


void Scene::deleteSkybox()
{
    assert(mSkyboxVertexArray.location != 0);
    deleteVertexArray(mSkyboxVertexArray);
    deleteVertexBuffer(mSkyboxVertexBuffers);
    mSkybox = SkyBox();
    mSkyboxVertexArray = VertexArray();
    mSkyboxVertexBuffers = VertexBufferModel();
    mDirectionalLights.erase(mSunDirectionalLightId);
}


void Scene::deleteEntity(int index)
{
    assert(mEntities.find(index) != mEntities.end());
    assert(mEntityResourceIndices.find(index) != mEntityResourceIndices.end());

    // Check if we can delete its vertex buffer and/or vertex array.
    deleteResources(mEntityResourceIndices[index]);

    // Delete the entity
    mEntities.erase(index);
    mEntityResourceIndices.erase(index);
}


void Scene::deletePointLight(int index)
{
    assert(mPointLights.find(index) != mPointLights.end());
    mPointLights.erase(index);
}


void Scene::deleteDirectionalLight(int index)
{
    assert(mDirectionalLights.find(index) != mDirectionalLights.end());
    mDirectionalLights.erase(index);
}


int Scene::addEntity(const Entity& entity)
{
    const IndexMesh& mesh = entity.getGeometry();

    // Create vertex buffer and array.
    VertexBufferModel vbo = createVertexBuffer(mesh);
    VertexArray vao = mPhong.createVertexArray(vbo);

    mEntities[mCurrentEntityId] = entity;
    mEntityVertexBuffers[mCurrentVertexBufferId] = vbo;
    mEntityVertexArrays[mCurrentVertexArrayId] = vao;
    mEntityResourceIndices[mCurrentEntityId] = EntityResourceIndices(mCurrentVertexArrayId, mCurrentVertexBufferId);

    mCurrentEntityId++;
    mCurrentVertexBufferId++;
    mCurrentVertexArrayId++;

    return mCurrentEntityId - 1;
}


int Scene::addEntityFromFile(const std::string& filename)
{
    const IndexMesh& mesh = mFileLoader.loadFromFile(filename);

    // Find the vertex array and vertex buffer locations for the drawing the mesh.
    mEntities[mCurrentEntityId] = Entity(mesh);
	mEntities[mCurrentEntityId].state = Entity::State::Dynamical;
	if (mesh.shader == Shader::Type::Water) {
		mEntities[mCurrentEntityId].state = Entity::State::Water;
	}
    EntityResourceIndices indices;

    // Find vertex buffer.
    auto cacheSearchVBO = mCachedVertexBuffers.find(filename);
    if (cacheSearchVBO == mCachedVertexBuffers.end()) {
        // Load the buffer and add it to the cache.
        mEntityVertexBuffers[mCurrentVertexBufferId] = createVertexBuffer(mesh);
        mCachedVertexBuffers[filename] = mCurrentVertexBufferId;
        indices.vertexBuffer = mCurrentVertexBufferId;
        mCurrentVertexBufferId++;
    }
    else {
        indices.vertexBuffer = cacheSearchVBO->second;
    }

    // Find vertex array.
    auto cacheSearchVAO = mCachedVertexArrays.find(filename);
    if (cacheSearchVAO == mCachedVertexArrays.end()) {
        VertexArray vertexArray;
        if (mesh.shader == Shader::Type::Phong) {
            vertexArray = mPhong.createVertexArray(mEntityVertexBuffers[indices.vertexBuffer]);
        }
        else if (mesh.shader == Shader::Type::Water) {
            vertexArray = mWater.createVertexArray(mEntityVertexBuffers[indices.vertexBuffer]);
        }
        else {
            // By defualt give it the phong shader.
            vertexArray = mPhong.createVertexArray(mEntityVertexBuffers[indices.vertexBuffer]);
        }
        mEntityVertexArrays[mCurrentVertexArrayId] = vertexArray;
        mCachedVertexArrays.emplace(filename, mCurrentVertexArrayId);
        indices.vertexArray = mCurrentVertexArrayId;
        mCurrentVertexArrayId++;
    }
    else {
        indices.vertexArray = cacheSearchVAO->second;
    }

    mEntityResourceIndices[mCurrentEntityId] = indices;
    mCurrentEntityId++;
    return mCurrentEntityId - 1;
}


int Scene::addPointLight(const PointLight& light)
{
    mPointLights[mCurrentPointLightId] = light;
    mCurrentPointLightId++;
    return mCurrentPointLightId - 1;
}


int Scene::addDirectionalLight(const DirectionalLight& light)
{
    mDirectionalLights[mCurrentDirectionalLightId] = light;
    mCurrentDirectionalLightId++;
    return mCurrentDirectionalLightId - 1;
}


std::map<int, Entity>::iterator Scene::getEntityFirst()
{
    return mEntities.begin();
}


std::map<int, Entity>::iterator Scene::getEntityLast()
{
    return mEntities.end();
}


void Scene::drawTerrain(float time, const Mat4& view, const Mat4& projection)
{
    if (mTerrainVertexArray.location == 0) {
        // We dont have a terrain.
        return;
    }

    // Use the phong shader for the terrain.
    mPhong.bind();
    mPhong.setViewMatrix(view);
    mPhong.setProjectionMatrix(projection);
    mPhong.setModelMatrix(mTerrain.getModelMatrix());
    mPhong.setLights(mPointLights, mDirectionalLights);

    glBindVertexArray(mTerrainVertexArray.location);

    for (std::size_t draw = 0; draw < mTerrainVertexArray.drawing.mode.size(); draw++) {

        std::size_t materialIndex = mTerrainVertexArray.drawing.material[draw];
        mPhong.setMaterial(mTerrain.getGeometry().materials[materialIndex]);

        glDrawArrays(
            mTerrainVertexArray.drawing.mode[draw], 
            mTerrainVertexArray.drawing.first[draw], 
            mTerrainVertexArray.drawing.count[draw]
        );

    }

    glCheckError();
}


void Scene::drawSkybox(float time, const Mat4& view, const Mat4& projection)
{
    if (mSkyboxVertexArray.location == 0) {
        // We have no skybox.
        return;
    }

    mSky.bind();
    mSky.setViewMatrix(view);
    mSky.setProjectionMatrix(projection);
    mSky.setModelMatrix(Mat4::identity());
    mSky.setTimeOfDay(mSkybox.getTimeOfDay(time));
    mSky.setSun(mSkybox.getSunLight(time));

    glBindVertexArray(mSkyboxVertexArray.location);

    for (std::size_t draw = 0; draw < mSkyboxVertexArray.drawing.mode.size(); draw++) {
        glDrawArrays(
            mSkyboxVertexArray.drawing.mode[draw], 
            mSkyboxVertexArray.drawing.first[draw], 
            mSkyboxVertexArray.drawing.count[draw]
        );
    }

    glCheckError();
}


void Scene::drawEntities(float time, const Mat4& view, const Mat4& projection)
{
    // Draw entities that has the phong shader.
    mPhong.bind();
    mPhong.setViewMatrix(view);
    mPhong.setProjectionMatrix(projection);
    mPhong.setLights(mPointLights, mDirectionalLights);
    GLuint currentVertexArray = 0;
    for (auto& pair : mEntities) {

        int id = pair.first;
        Entity& entity = pair.second;
        EntityResourceIndices& indices = mEntityResourceIndices[id];
        VertexArray& vertexArray = mEntityVertexArrays[indices.vertexArray];

        if (vertexArray.shader != Shader::Type::Phong) {
            continue;
        }

        mPhong.setModelMatrix(entity.getModelMatrix());

        if (vertexArray.location != currentVertexArray) {
            glBindVertexArray(vertexArray.location);
            currentVertexArray = vertexArray.location;
        }

        for (std::size_t draw = 0; draw < vertexArray.drawing.material.size(); draw++) {
            std::size_t materialIndex = vertexArray.drawing.material[draw];
            mPhong.setMaterial(entity.getGeometry().materials[materialIndex]);
            glDrawArrays(
                vertexArray.drawing.mode[draw], 
                vertexArray.drawing.first[draw], 
                vertexArray.drawing.count[draw]
            );
        }
    }

    // Draw entites that has the water shader. This requires blending.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    mWater.bind();
    mWater.setViewMatrix(view);
    mWater.setProjectionMatrix(projection);
    mWater.setLights(mPointLights, mDirectionalLights);
    mWater.setTime(time);
    mWater.setTimeOfDay(mSkybox.getTimeOfDay(time));
    for (auto& pair : mEntities) {

        int id = pair.first;
        Entity& entity = pair.second;
        EntityResourceIndices& indices = mEntityResourceIndices[id];
        VertexArray& vertexArray = mEntityVertexArrays[indices.vertexArray];

        if (vertexArray.shader != Shader::Type::Water) {
            continue;
        }

        mWater.setModelMatrix(entity.getModelMatrix());

        if (vertexArray.location != currentVertexArray) {
            glBindVertexArray(vertexArray.location);
            currentVertexArray = vertexArray.location;
        }

        for (std::size_t draw = 0; draw < vertexArray.drawing.material.size(); draw++) {
            std::size_t materialIndex = vertexArray.drawing.material[draw];
            mWater.setMaterial(entity.getGeometry().materials[materialIndex]);
            glDrawArrays(
                vertexArray.drawing.mode[draw], 
                vertexArray.drawing.first[draw], 
                vertexArray.drawing.count[draw]
            );
        }

    }
    glDisable(GL_BLEND);

    glCheckError();
}


void Scene::deleteResources(const EntityResourceIndices& entityResource)
{
    // Check if we need to delete the vertex array and buffer.
    bool foundBufferReference = false;
    bool foundArrayReference = false;

    for (const auto& resource : mEntityResourceIndices) {
        if (resource.second.vertexBuffer == entityResource.vertexBuffer) {
            foundBufferReference = true;
            if (foundArrayReference) {
                break;
            }
        }
        if (resource.second.vertexArray == entityResource.vertexArray) {
            foundArrayReference = true;
            if (foundBufferReference) {
                break;
            }
        }
    }

    // Check if the vertex buffer is in cache.
    for (auto& cache : mCachedVertexBuffers) {
        if (cache.second == entityResource.vertexBuffer) {
            foundBufferReference = true;
            break;
        }
    }

    // Check if the vertex array is in cache.
    for (auto& cache : mCachedVertexArrays) {
        if (cache.second == entityResource.vertexArray) {
            foundArrayReference = true;
            break;
        }
    }
    
    // Delete them.
    if (!foundArrayReference) {
        deleteVertexArray(mEntityVertexArrays[entityResource.vertexArray]);
        mEntityVertexArrays.erase(entityResource.vertexArray);
    }
    if (!foundBufferReference) {
        deleteVertexBuffer(mEntityVertexBuffers[entityResource.vertexBuffer]);
        mEntityVertexBuffers.erase(entityResource.vertexBuffer);
    }
}