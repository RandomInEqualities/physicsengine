
#include <vector>
#include <cstddef>
#include <cassert>
#include <iostream>
#include "SkyBox.hpp"
#include "Geometry.hpp"
#include "Lights.hpp"
#include "Math/Vec3.hpp"
#include "Math/Math.hpp"
#include "Math/Debug.hpp"


SkyBox::SkyBox() : 
    mCenter(0,0,0), 
    mBoxRadius(100.0f)
{

}


const IndexMesh& SkyBox::getGeometry() const
{
    return mMesh;
}


float SkyBox::getTimeOfDay(float time) const
{
    return Math::modulus((time + 12), 24);
}


DirectionalLight SkyBox::getSunLight(float time) const
{
    DirectionalLight sun;

    // Find the direction of the sun.
    float timeOfDay = getTimeOfDay(time);
    float sunAngle = Math::PI*(timeOfDay - 12.0f)/12.0f;
    sun.direction = Vec3(Math::sin(sunAngle), 0.0f, Math::cos(sunAngle));

    // Find the colour of the sun.
    Vec3 middaySunColor(1.0f, 0.9f, 0.6f);
    Vec3 sunsetSunColor(1.0f, 0.5f, 0.2f);
    float sunDotUp = dot(sun.direction, Vec3(0,0,1));
    if (sunDotUp < 0) {
        sun.diffuse = std::max(0.0f, (0.6f + sunDotUp)) * sunsetSunColor;
        sun.specular = sun.diffuse/3;
        return sun;
    }
    sun.diffuse = sunDotUp * middaySunColor + (1.0f - sunDotUp) * sunsetSunColor;
    sun.specular = sun.diffuse/5;
    return sun;
}


void SkyBox::generate(float zFar)
{
    // Sidelength of box must be zFar/sqrt(3) so that all corners are contained in 
    // the frustrum (Derived from a^2+a^2+a^2 = c^2)
    mBoxRadius = zFar*0.5f;

    // Corners in the box.
    mMesh.vertices.emplace_back(mCenter.x-mBoxRadius, mCenter.y-mBoxRadius, mCenter.z-mBoxRadius);
    mMesh.vertices.emplace_back(mCenter.x+mBoxRadius, mCenter.y-mBoxRadius, mCenter.z-mBoxRadius);
    mMesh.vertices.emplace_back(mCenter.x-mBoxRadius, mCenter.y+mBoxRadius, mCenter.z-mBoxRadius);
    mMesh.vertices.emplace_back(mCenter.x+mBoxRadius, mCenter.y+mBoxRadius, mCenter.z-mBoxRadius);

    mMesh.vertices.emplace_back(mCenter.x-mBoxRadius, mCenter.y-mBoxRadius, mCenter.z+mBoxRadius);
    mMesh.vertices.emplace_back(mCenter.x+mBoxRadius, mCenter.y-mBoxRadius, mCenter.z+mBoxRadius);
    mMesh.vertices.emplace_back(mCenter.x-mBoxRadius, mCenter.y+mBoxRadius, mCenter.z+mBoxRadius);
    mMesh.vertices.emplace_back(mCenter.x+mBoxRadius, mCenter.y+mBoxRadius, mCenter.z+mBoxRadius);

    // Normals.
    mMesh.normals.emplace_back(0.0f,0.0f,1.0f);
    mMesh.normals.emplace_back(0.0f,0.0f,-1.0f);
    mMesh.normals.emplace_back(0.0f,1.0f,0.0f);
    mMesh.normals.emplace_back(0.0f,-1.0f,0.0f);
    mMesh.normals.emplace_back(1.0f,0.0f,0.0f);
    mMesh.normals.emplace_back(-1.0f,0.0f,0.0f);

    // Texture coordinates.
    mMesh.texCoords.emplace_back(0.0f,1.0f);
    mMesh.texCoords.emplace_back(1.0f,0.0f);
    mMesh.texCoords.emplace_back(1.0f,1.0f);

    // 6 sides, 12 triangles, 36 points
    mMesh.triangles.emplace_back(constructTriangle(0, 1, 2, 0, 0));
    mMesh.triangles.emplace_back(constructTriangle(2, 1, 3, 0, 0));

    mMesh.triangles.emplace_back(constructTriangle(5, 4, 6, 1, 0));
    mMesh.triangles.emplace_back(constructTriangle(5, 6, 7, 1, 0));

    mMesh.triangles.emplace_back(constructTriangle(1, 0, 4, 2, 0));
    mMesh.triangles.emplace_back(constructTriangle(1, 4, 5, 2, 0));

    mMesh.triangles.emplace_back(constructTriangle(2, 3, 6, 3, 0));
    mMesh.triangles.emplace_back(constructTriangle(6, 3, 7, 3, 0));

    mMesh.triangles.emplace_back(constructTriangle(0, 2, 4, 4, 0));
    mMesh.triangles.emplace_back(constructTriangle(4, 2, 6, 4, 0));

    mMesh.triangles.emplace_back(constructTriangle(3, 1, 5, 5, 0));
    mMesh.triangles.emplace_back(constructTriangle(3, 5, 7, 5, 0));

    // Set the shader and material
    Material material;
    material.ambient = Vec3(0.2f, 0.0f, 0.0f);
    material.diffuse = Vec3(0.3f, 0.3f, 0.0f);
    material.specular = Vec3(0.0f, 0.2f, 0.2f);
    material.shininess = 20;
    mMesh.materials.emplace_back(material);
    mMesh.shader = Shader::Type::Sky;

    assert(!mMesh.haveOutOfRangeIndices());
}


IndexTriangle SkyBox::constructTriangle(std::size_t v0, std::size_t v1, std::size_t v2, 
                                        std::size_t normal, std::size_t material)
{
    IndexTriangle triangle;
    triangle.v0 = v0;
    triangle.v1 = v1;
    triangle.v2 = v2;
    triangle.v0normal = normal;
    triangle.v1normal = normal;
    triangle.v2normal = normal;
    triangle.v0texCoord = 0;
    triangle.v1texCoord = 1;
    triangle.v2texCoord = 2;
    triangle.material = material;
    return triangle;
}