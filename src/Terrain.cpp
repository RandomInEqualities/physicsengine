#include <vector>
#include <string>
#include <iostream>
#include <cassert>

#include "Terrain.hpp"
#include "Geometry.hpp"
#include "Shader.hpp"
#include "Math/Vec2.hpp"
#include "Math/Vec3.hpp"
#include "Math/Mat4.hpp"
#include "Math/Math.hpp"
#include "Math/Debug.hpp"


Terrain::Terrain() : 
    mCenter(0,0,0), 
    mPointsX(0), 
    mPointsY(0), 
    mTriangleSize(0.0f), 
    mElasticCoefficient(0.0f)
{

}


void Terrain::generate(Vec3 center, int pointsX, int pointsY, float triangleSize, int type)
{
    using Math::PI;
    using Math::PIOVER2;

    mCenter = center;
    mPointsX = pointsX;
    mPointsY = pointsY;
    mTriangleSize = triangleSize;

    // Set triangle points in a wavy sine/cosine map.
    Vec2 size(pointsX * triangleSize, pointsY * triangleSize);
    for (int i = 0; i < pointsX; i++) {
        for (int j = 0; j < pointsY; j++) {
            float x = i*triangleSize - size.x/2.0f;
            float y = j*triangleSize - size.y/2.0f;
            float z = 0;
            switch (type) {
                case 1:
                    z = 2500 +  (- i - j)*10.0f;
                    break;
                case 2:
                    z = -30 + 100 * sin(i/(8*PI))*cos(j/(6*PI)) + cos(j/(12*PI)) + 0.4f*cos(j/(3*PIOVER2)+i/(8*PI));
                    break;
                case 3:
                    z = -10 + -0.8f*(i-pointsX/2)*(i-pointsX/2) + 0.003f*(i-pointsX/2)*(i-pointsX/2)*(i-pointsX/2)*(i-pointsX/2) + j;
                    break;
                case 4:
                    z = 0;
                    break;
            }
            mMesh.vertices.emplace_back(x, y, z);
        }
    }

    // Set triangle texture coordinates.
    mMesh.texCoords.emplace_back(1.0f,0.0f);
    mMesh.texCoords.emplace_back(0.0f,1.0f);
    mMesh.texCoords.emplace_back(1.0f,1.0f);

    // Set triangle indices.
    for (int i = 0; i < pointsX - 1; i++) {
        for (int j = 0; j < pointsY - 1; j++) {

            // Make 2 triangles for each 4 points (square)

            // low triangle
            IndexTriangle triangle1;
            triangle1.v0 = (i+0) + pointsX*(j+0);
            triangle1.v1 = (i+1) + pointsX*(j+0);
            triangle1.v2 = (i+0) + pointsX*(j+1);
            triangle1.v0normal = triangle1.v0;
            triangle1.v1normal = triangle1.v1;
            triangle1.v2normal = triangle1.v2;
            triangle1.v0texCoord = 0;
            triangle1.v1texCoord = 1;
            triangle1.v2texCoord = 2;
            triangle1.material = 0;

            // high triangle
            IndexTriangle triangle2;
            triangle2.v0 = (i+1) + pointsX*(j+1);
            triangle2.v1 = (i+0) + pointsX*(j+1);
            triangle2.v2 = (i+1) + pointsX*(j+0);
            triangle2.v0normal = triangle2.v0;
            triangle2.v1normal = triangle2.v1;
            triangle2.v2normal = triangle2.v2;
            triangle2.v0texCoord = 0;
            triangle2.v1texCoord = 1;
            triangle2.v2texCoord = 2;
            triangle2.material = 0;

            mMesh.triangles.emplace_back(triangle1);
            mMesh.triangles.emplace_back(triangle2);
        }
    }

    // Compute normals for each index location.
    for (int i = 0; i < pointsX; i++) {
        for (int j = 0; j < pointsY; j++) {
            Vec3 normalSum(0,0,0);
            int count = 0;
            for (int pi = -1; pi <= 1; pi++) {
                for (int pj = -1; pj <= 1; pj++) {

                    int index0 = (i+pi+0) + pointsX*(j+pj+0);
                    int index1 = (i+pi+1) + pointsX*(j+pj+0);
                    int index2 = (i+pi+0) + pointsX*(j+pj+1);

                    if (index0 < 0 || (int)mMesh.vertices.size() <= index0) {
                        continue;
                    }
                    if (index1 < 0 || (int)mMesh.vertices.size() <= index1) {
                        continue;
                    }
                    if (index2 < 0 || (int)mMesh.vertices.size() <= index2) {
                        continue;
                    }

                    normalSum += computeNormal(
                        mMesh.vertices[static_cast<size_t>(index0)], 
                        mMesh.vertices[static_cast<size_t>(index1)], 
                        mMesh.vertices[static_cast<size_t>(index2)]
                    );
                    count++;
                }
            }
            if (count > 0) {
                mMesh.normals.emplace_back(normalSum/(float)count);
            }
        }
    }

    // Set shader and material.
    mMesh.shader = Shader::Type::Phong;
    Material material;
    material.ambient = Vec3(0.05f, 0.05f, 0.025f);
    material.diffuse = Vec3(0.3f, 0.4f, 0.3f);
    material.specular = Vec3(0.2f, 0.2f, 0.2f);
    material.shininess = 20;
    mMesh.materials.emplace_back(material);

    assert(!mMesh.haveOutOfRangeIndices());
}


const IndexMesh& Terrain::getGeometry() const
{
    return mMesh;
}

Mat4 Terrain::getModelMatrix() const
{
    return Mat4::translate(-mCenter);
}


float Terrain::getTerrainCoordinate(float worldX) const {
    // Transform a world coordinate into terrain coordinate
    float sizeX = mPointsX * mTriangleSize;
    return (sizeX/2.0f - worldX - mCenter.x)/mTriangleSize;
}


float Terrain::getHeight(float worldY, float worldX) const {

    // Relative point on the pointmap
    float relX = getTerrainCoordinate(worldX);
    float relY = getTerrainCoordinate(worldY);

    // Throw away decimals to find the square we are working in
    int squareX = (int) relX;
    int squareY = (int) relY;

    // Return lowest possible value if the point does not exist on our terrain
    if (squareX < 0 || squareY < 0 || squareX >= mPointsX - 1 || squareY >= mPointsY - 1) {
        return -FLT_MAX;
    }

    // heights
    float h00 = mCenter.z - mMesh.vertices[squareX + mPointsX*squareY].z;
    float h11 = mCenter.z - mMesh.vertices[(squareX+1) + mPointsX*(squareY+1)].z;
    Vec3 normal = getLocalNormal(worldY,worldX);

    // Figure out which triangle of the square we use
    // find the height by using the plane squareP+normal*relativeP
    if ((relX-squareX)+(relY-squareY) < 1.0f) {
        // Low triangle
        return (h00 - mTriangleSize*((relY-squareY)*-normal.x + (relX-squareX)*-normal.y)/normal.z);
    } 
    else { 
        // High triangle
        return (h11 - mTriangleSize*((-1+relY-squareY)*-normal.x + (-1+relX-squareX)*-normal.y)/normal.z);
    }

}


Vec3 Terrain::getNormal(float worldY, float worldX) const
{

    //Relative point on the pointmap
    float relX = getTerrainCoordinate(worldX);
    float relY = getTerrainCoordinate(worldY);

    //Throw away decimals to find the square we are working in
    int squareX = (int) relX;
    int squareY = (int) relY;

    //Return (0,0,1) normal vector if the point does not exist on our terrain
    if (squareX < 0 || squareY < 0 || squareX >= mPointsX - 1 || squareY >= mPointsY - 1) {
        return Vec3(0,0,1);
    }

    //4 points in the square
    Vec3 p00 = mMesh.vertices[squareX + mPointsX*squareY];
    Vec3 p10 = mMesh.vertices[(squareX+1) + mPointsX*(squareY+0)];
    Vec3 p01 = mMesh.vertices[(squareX+0) + mPointsX*(squareY+1)];
    Vec3 p11 = mMesh.vertices[(squareX+1) + mPointsX*(squareY+1)];

    //Figure out which triangle of the square we use and return normalized normal
    //return cross product of 
    if ((relX-squareX)+(relY-squareY) < 1.0f) {
        //Low triangle
        return cross((p01-p00),(p10-p00)).normalize();
    } 
    else { 
        //High triangle
        return cross((p10-p11),(p01-p11)).normalize();
    }
}


Vec3 Terrain::getLocalNormal(float worldY, float worldX) const {

    // Relative point on the pointmap
    float relX = getTerrainCoordinate(worldX);
    float relY = getTerrainCoordinate(worldY);

    // Throw away decimals to find the square we are working in
    int squareX = (int) relX;
    int squareY = (int) relY;

    // Return (0,0,1) normal vector if the point does not exist on our terrain
    if (squareX < 0 || squareY < 0 || squareX >= mPointsX - 1 || squareY >= mPointsY - 1) {
        return Vec3(0,0,1);
    }

    // 4 points in the square
    Vec3 p00 = mCenter + mMesh.vertices[squareX + mPointsX*squareY];
    Vec3 p10 = mCenter + mMesh.vertices[(squareX+1) + mPointsX*(squareY+0)];
    Vec3 p01 = mCenter + mMesh.vertices[(squareX+0) + mPointsX*(squareY+1)];
    Vec3 p11 = mCenter + mMesh.vertices[(squareX+1) + mPointsX*(squareY+1)];

    // Figure out which triangle of the square we use and return normalized normal
    // return cross product of 
    if ((relX-squareX)+(relY-squareY) < 1.0f) {
        // Low triangle
        return Vec3(cross((p01-p00),(p10-p00)).normalize());
    } 
    else { 
        // High triangle
        return Vec3(cross((p10-p11),(p01-p11)).normalize());
    }

}