#ifndef ENTITY_HPP_INCLUDED
#define ENTITY_HPP_INCLUDED

#include "Geometry.hpp"
#include "Math/Vec3.hpp"
#include "Math/Mat3.hpp"


class Entity
{
public:

    enum class State 
    {
        Statical,
        Dynamical,
        UnCollidable,
        Water
    };

public:

    // Entity with no mesh.
    Entity();
    Entity(const IndexMesh& mesh);
    Entity(const Entity& entity, const IndexMesh& newMesh);

    const IndexMesh& getGeometry() const;
    Mat4 getModelMatrix() const;
    Vec3 getInertia() const;

    // Set one material to use for the whole mesh.
    void setMaterial(const Material& material);

    // Set a new material vector. The new material vector must be as long 
    // or longer than the existing one.
    void setMaterial(const std::vector<Material>& materials);

    // Keep public for easy access, add getters when done :).
    float elasticCoefficient;
    float mass;
    float boundingRadius;
    Vec3 position;
    Vec3 velocityLinear;
    Vec3 velocityAngular;
    Mat3 rotation;
    State state;

private:

    static float computeBoundingRadius(const IndexMesh& mesh);
    IndexMesh mMesh;

};


#endif // ENTITY_HPP_INCLUDED

