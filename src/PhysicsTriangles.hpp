#ifndef PHYSICS_TRIANGLES_HPP_INCLUDED
#define PHYSICS_TRIANGLES_HPP_INCLUDED

#include "Physics.hpp"
#include "Math/Vec3.hpp"
class Entity;
class Scene;
class Terrain;


/*
    Physics class that does accurate triangle-triangle collisions.
    It not finished yet....
*/
class PhysicsTriangles : public Physics
{
public:

    PhysicsTriangles();

    void setGravity(Vec3 gravity) override;
    void update(float deltaTime, Scene& scene) override;

private:

    bool collide(const Entity& entityA, const Entity& entityB);
    bool collide(const Entity& entity, Terrain& terrain);
    void impulseInelastic(Entity& entityA, Entity& entB);
    void impulseInelastic(Entity& entity, Terrain& terrain);

    Vec3 gravity;
};


#endif // PHYSICS_TRIANGLES_HPP_INCLUDED