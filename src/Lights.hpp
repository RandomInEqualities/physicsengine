#ifndef LIGHTS_HPP_INCLUDED
#define LIGHTS_HPP_INCLUDED

#include "Math/Vec3.hpp"


class PointLight
{
public:

    PointLight();
    PointLight(Vec3 position, Vec3 diffuse, Vec3 specular);

    Vec3 position;
    Vec3 diffuse;
    Vec3 specular;

};


class DirectionalLight
{
public:

    DirectionalLight();
    DirectionalLight(Vec3 direction, Vec3 diffuse, Vec3 specular);

    Vec3 direction;
    Vec3 diffuse;
    Vec3 specular;

};


#endif