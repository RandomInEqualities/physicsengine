#ifndef SKY_BOX_HPP_INCLUDED
#define SKY_BOX_HPP_INCLUDED

#include <vector>
#include <cstddef>
#include "Geometry.hpp"
#include "Math/Vec3.hpp"
class DirectionalLight;


class SkyBox
{
public:

    SkyBox();

    const IndexMesh& getGeometry() const;
    float getTimeOfDay(float time) const;
    void generate(float zFar);
    DirectionalLight getSunLight(float time) const;

private:

    static IndexTriangle constructTriangle(
        std::size_t v0, 
        std::size_t v1, 
        std::size_t v2, 
        std::size_t normal, 
        std::size_t material
    );

    Vec3 mCenter;
    float mBoxRadius;
    IndexMesh mMesh;

};


#endif // SKY_BOX_HPP_INCLUDED
