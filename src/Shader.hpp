#ifndef SHADER_HPP_INCLUDED
#define SHADER_HPP_INCLUDED

#include <string>
#include <vector>

#include <GL/glew.h>
class VertexBufferModel;
class VertexArray;


// Abstrat shader class that acts as a interface for different shaders. TODO: =delete
// the copy constructor and assignment when upgrading to VS2013+. Do not try to copy this
// object, as the destructor will destroy the shader program.
class Shader
{
public:

    // The different shaders that we have implemented.
    enum class Type
    {
        None,
        Phong,
        Sky,
        Water
    };

    Shader();
    ~Shader();

    // Create or recreate the shader. Throws runtime error on failure.
    virtual void compile() = 0;

    // Bind the shader program.
    void bind();

    // Has the shader been compiled succesfully?
    bool ready();

    // Setup a vertex array to glue together vertex buffers and this shaders attributes.
    virtual VertexArray createVertexArray(const VertexBufferModel& model) = 0;

protected:

    // Load and compile a shader program from a vertex and fragment shader file. Throws a runtime
    // exception if something goes wrong.
    static GLuint loadShaderFromFile(const std::string& vertexFilename, const std::string& fragmentFilename);

    // Load and compile a shader program from vertex and fragment shader source code. Throws a runtime
    // exception if something goes wrong.
    static GLuint loadShaderFromMemory(const std::string& vertexCode, const std::string& fragmentCode);

    // Find attributes and uniforms in a shader. Returns -1 if it fails to find it.
    static GLint findAttributeLocation(GLuint program, const std::string& name);
    static GLint findUniformLocation(GLuint program, const std::string& name);

protected:

    GLuint mShaderProgram;

private:

    static std::vector<GLchar> getFileContents(const std::string& filename);
    static GLuint compile(const GLchar* vertexCode, const GLchar* fragmentCode);

};


#endif