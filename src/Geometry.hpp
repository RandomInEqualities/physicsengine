#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include <cstddef>
#include <vector>

#include "Math/Vec2.hpp"
#include "Math/Vec3.hpp"
#include "Shader.hpp"


class Material
{
public:
    // Sets the material to white.
    Material();
    Material(Vec3 ambient, Vec3 diffuse, Vec3 specular, float shininess);

    Vec3 ambient;
    Vec3 diffuse;
    Vec3 specular;
    float shininess;
};


class Triangle 
{
public:
    Vec3 v0;
    Vec3 v1;
    Vec3 v2;

    Vec3 v0normal;
    Vec3 v1normal;
    Vec3 v2normal;

    Vec2 v0texCoord;
    Vec2 v1texCoord;
    Vec2 v2texCoord;

    Material material;
};


class IndexTriangle
{
public:

    // Set indices to zero.
    IndexTriangle();

    std::size_t v0;
    std::size_t v1;
    std::size_t v2;

    std::size_t v0normal;
    std::size_t v1normal;
    std::size_t v2normal;

    std::size_t v0texCoord;
    std::size_t v1texCoord;
    std::size_t v2texCoord;

    std::size_t material;
};


class IndexMesh
{
public:

    // Empty mesh.
    IndexMesh();

    // Initial mesh.
    IndexMesh(
        const std::vector<IndexTriangle>& triangles,
        const std::vector<Vec3>& vertices,
        const std::vector<Vec3>& normals,
        const std::vector<Vec2>& texCoords,
        const std::vector<Material>& materials
    );

    std::vector<IndexTriangle> triangles;
    std::vector<Vec3> vertices;
    std::vector<Vec3> normals;
    std::vector<Vec2> texCoords;
    std::vector<Material> materials;

    // The shader to use for the model.
    Shader::Type shader;

    // Check if any of the indices in the triangle is out of range.
    bool haveOutOfRangeIndices() const;

};


// Compute normal for 3 triangle vertices.
Vec3 computeNormal(Vec3 v0, Vec3 v1, Vec3 v2);


// Convert between index meshes and simple triangle meshes.
std::vector<Triangle> convertToSimpleMesh(const IndexMesh& mesh);
IndexMesh convertToIndexMesh(const std::vector<Triangle>& mesh);


bool triangleLineIntersection(Vec3& position, Vec3 a, Vec3 b, Vec3 c, Vec3 lineP1, Vec3 lineP2);
bool trianglePlaneIntersection(Vec3& position, Vec3 a, Vec3 b, Vec3 c, Vec3 lineP1, Vec3 lineP2);
bool pointInTriangle(Vec3 position, Vec3 a, Vec3 b, Vec3 c);
bool triangleTriangleIntersection(Vec3& position, Vec3 a0, Vec3 b0, Vec3 c0, Vec3 a1, Vec3 b1, Vec3 c1);


#endif