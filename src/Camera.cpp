
#include "Camera.hpp"
#include "Math/Math.hpp"
#include "Math/Vec3.hpp"
#include "Math/Vec4.hpp"
#include "Math/Mat4.hpp"

#include <iostream>
#include "Math/Debug.hpp"


Camera::Camera() :
    mPosition(0.0f, 0.f, 0.f),
    mUp(0.0f, 0.0f, 1.0f),
    mSpeed(20.0f),
    mPitch(Math::PI),
    mYaw(-Math::PI),
    mFieldOfViewY(90.0f),
    mFarZ(5000.0f),
    mNearZ(0.1f)
{

}


void Camera::setLocation(Vec3 position, Vec3 lookat)
{
    mPosition = position;
    Vec3 lookDir = (lookat - position).normalize();
    mPitch = Math::acos(lookDir.z);
    mYaw = Math::acos(lookDir.y/Math::sin(mPitch));
}


void Camera::strafeLeft(float deltaTime)
{
    move(cross(getLookDirection(), mUp).normalize(), deltaTime);
}


void Camera::strafeRight(float deltaTime)
{
    move(-cross(getLookDirection(), mUp).normalize(), deltaTime);
}


void Camera::moveForward(float deltaTime)
{
    move(getLookDirection(), deltaTime);
}


void Camera::moveBackward(float deltaTime)
{
    move(-getLookDirection(), deltaTime);
}


void Camera::move(Vec3 direction, float deltaTime)
{
    mPosition += deltaTime * mSpeed * direction;
}


void Camera::rotate(float deltaX, float deltaY) {

    mYaw -= Math::PIOVER2 * deltaX;
    mPitch += Math::PIOVER2 * deltaY;

    // Restrict the pitch, so the camera does not loop around itself.
    float epsilon = 0.01f;
    Math::clamp(mPitch, epsilon, Math::PI - epsilon);
}


Vec3 Camera::getPosition() const
{
    return mPosition;
}


Vec3 Camera::getUp() const 
{
    return mUp;
}


Vec3 Camera::getLookDirection() const {
    return Vec3 (
        sin(mPitch)*sin(mYaw),
        sin(mPitch)*cos(mYaw),
        cos(mPitch)
    );
}


float Camera::getSpeed() const 
{
    return mSpeed;
}


Mat4 Camera::getViewMatrix() const {
    return Mat4::rotateX(mPitch) * Mat4::rotateZ(mYaw) * Mat4::translate(mPosition);
}


float Camera::getFieldOfViewY() const {
    return mFieldOfViewY;
}


float Camera::getFarZ() const {
    return mFarZ;
}


float Camera::getNearZ() const {
    return mNearZ;
}

