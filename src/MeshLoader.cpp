////////////////////////////////////////////////////////////////////////
//
// ResourceLoader.cpp
//
////////////////////////////////////////////////////////////////////////



#include <vector>
#include <string>
#include <map>
#include <cstddef>
#include <stdexcept>
#include <iostream>

#include "MeshLoader.hpp"
#include "Geometry.hpp"
#include "FileFormats/WavefrontObj.hpp"



////////////////////////////////////////////////////////////////////////
//
// Private Methods
//
////////////////////////////////////////////////////////////////////////



namespace 
{


// Add triangle from a obj index face into a mesh. Throws out_of_range if something
// is wrong. Uses the last 3 texture coordinates as the default if the face has no
// texture coordinates. Computes a normal if it has no normals.
void addFaceTriangleToMesh(
    IndexMesh& mesh, 
    std::size_t faceIndex0, 
    std::size_t faceIndex1,
    std::size_t faceIndex2,
    const obj::IndexFace& face,
    const obj::Model& model
) {
    IndexTriangle tri;

    // Add vertex indices.
    tri.v0 = face.vertices.at(faceIndex0);
    tri.v1 = face.vertices.at(faceIndex1);
    tri.v2 = face.vertices.at(faceIndex2);

    // Add normal indices.
    if (face.normals.size() > 0) {
        tri.v0normal = face.normals.at(faceIndex0);
        tri.v1normal = face.normals.at(faceIndex1);
        tri.v2normal = face.normals.at(faceIndex2);
    } 
    else {
        Vec3 normal = computeNormal(
            model.vertices.at(tri.v0), 
            model.vertices.at(tri.v1), 
            model.vertices.at(tri.v2)
            );
        mesh.normals.emplace_back(normal);
        tri.v0normal = mesh.normals.size() - 1;
        tri.v1normal = mesh.normals.size() - 1;
        tri.v2normal = mesh.normals.size() - 1;
    }

    // Add texture coordinate indices.
    if (face.textureCoordinates.size() > 0) {
        tri.v0texCoord = face.textureCoordinates.at(faceIndex0);
        tri.v1texCoord = face.textureCoordinates.at(faceIndex1);
        tri.v2texCoord = face.textureCoordinates.at(faceIndex2);
    }
    else {
        if (mesh.texCoords.size() < 3) {
            mesh.texCoords.emplace_back(1.0f,0.0f);
            mesh.texCoords.emplace_back(0.0f,1.0f);
            mesh.texCoords.emplace_back(1.0f,1.0f);
        }
        tri.v0texCoord = mesh.texCoords.size() - 3;
        tri.v1texCoord = mesh.texCoords.size() - 2;
        tri.v2texCoord = mesh.texCoords.size() - 1;
    }

    // Add material index.
    tri.material = face.material;

    mesh.triangles.emplace_back(std::move(tri));
}


// Convert an obj::material to a material that we can use.
Material convertMaterial(const obj::Material& material)
{
    Material result;
    result.ambient = material.ambient;
    result.diffuse = material.diffuse;
    result.specular = material.specular;
    result.shininess = material.shininess;
    return result;
}


} // anonymous namespace.



////////////////////////////////////////////////////////////////////////
//
// ResourceLoader Public Methods
//
////////////////////////////////////////////////////////////////////////



const IndexMesh& MeshLoader::loadFromFile(const std::string& filename)
{
    // Check if we have already loaded it.
    if (cache.find(filename) != cache.end()) {
        return cache[filename];
    }

    obj::Model model;
    model.readFromFile(filename);

    // Copy the vertices, normals and texture coordinates.
    IndexMesh mesh;
    mesh.vertices = model.vertices;
    mesh.normals = model.normals;
    mesh.texCoords = model.textureCoordinates;
    mesh.shader = Shader::Type::Phong;

    // Add default texture coordinates that will be used if the face does not have
    // any.
    mesh.texCoords.emplace_back(1.0f,0.0f);
    mesh.texCoords.emplace_back(0.0f,1.0f);
    mesh.texCoords.emplace_back(1.0f,1.0f);

    // Convert the materials.
    for (std::size_t index = 0; index < model.materials.size(); index++) {
        mesh.materials.emplace_back(convertMaterial(model.materials[index]));

        // Choose a shader for the model depending on the last material illumination
        // parameter. We do not yet support models that need different shaders in its
        // rendering.
        int illumination = model.materials[index].illumination;
        if (illumination == 0) {
            mesh.shader = Shader::Type::Phong;
        }
        else if (illumination == 1) {
            mesh.shader = Shader::Type::Water;
        } 
        else {
            mesh.shader = Shader::Type::Phong;
        }
    }

    // Convert the faces.
    for (const obj::IndexFace& face : model.faces) {

        if (face.vertices.size() != 3 && face.vertices.size() != 4) {
            throw std::runtime_error(filename + " face is not a triangle or quad.");
        }

        // Construct triangles from the face.
        try {
            addFaceTriangleToMesh(mesh, 0, 1, 2, face, model);
            if (face.vertices.size() == 4) {
                addFaceTriangleToMesh(mesh, 2, 3, 0, face, model);
            }
        }
        catch (std::out_of_range) {
            throw std::runtime_error(filename + " face has invalid index.");
        }

    }

    if (mesh.haveOutOfRangeIndices()) {
        throw std::runtime_error(filename + " malformed obj mesh.");
    }

    // Store model in cache and return it.
    cache.emplace(filename, std::move(mesh));
    return cache[filename];
}