#ifndef PHYSICS_SPHERE_HPP_INCLUDED
#define PHYSICS_SPHERE_HPP_INCLUDED

#include "Physics.hpp"
#include "Math/Vec3.hpp"
class Terrain;
class Scene;
class Entity;


/*
    Physics class based in bounding sphere collisions and simple impulse response
    physics.
*/
class PhysicsSphere : public Physics
{
public:

    PhysicsSphere();
    ~PhysicsSphere();

    void setGravity(Vec3 gravity) override;
    void update(float deltaTime, Scene& scene) override;

private:

    bool collision(const Entity& entityity, Terrain& terrain);
    bool collision(const Entity& entityA, const Entity& entityB);
    bool contains(const Entity& entityA, const Entity& waterEntity);
    void impulseInelastic(Entity& entityA, Entity& entityB);
    void impulseInelastic(Entity& entity, Terrain& terrain);
    void waterResponse(Entity& entity);

    Vec3 gravity;
};

#endif // PHYSICS_SPHERE_HPP_INCLUDED