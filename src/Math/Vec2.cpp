////////////////////////////////////////////////////////////////////////
//
// Vec2.cpp
//
////////////////////////////////////////////////////////////////////////



#include <vector>
#include <cassert>
#include "Vec2.hpp"
#include "Math.hpp"



////////////////////////////////////////////////////////////////////////
// Vec2 public methods
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Vec2::Vec2()
{

}


////////////////////////////////////////////////////////////////////////
Vec2::Vec2(float initialX, float initialY)
{
    x = initialX;
    y = initialY;
}



////////////////////////////////////////////////////////////////////////
float Vec2::length() const
{
    return Math::sqrt(x*x + y*y);
}


////////////////////////////////////////////////////////////////////////
float Vec2::lengthSquare() const
{
    return x*x + y*y;
}


////////////////////////////////////////////////////////////////////////
Vec2& Vec2::normalize()
{
    float len = length();
    assert(!Math::isZero(len));
    x /= len;
    y /= len;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Vec2 Vec2::operator-() const
{
    return Vec2(-x, -y);
}


////////////////////////////////////////////////////////////////////////
Vec2 Vec2::operator+(Vec2 vector) const
{
    return Vec2(x + vector.x, y + vector.y);
}


////////////////////////////////////////////////////////////////////////
Vec2 Vec2::operator-(Vec2 vector) const
{
    return Vec2(x - vector.x, y - vector.y);
}


////////////////////////////////////////////////////////////////////////
Vec2& Vec2::operator+=(Vec2 vector)
{
    x += vector.x;
    y += vector.y;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Vec2& Vec2::operator-=(Vec2 vector)
{
    x -= vector.x;
    y -= vector.y;
    return *this;
}



////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Vec2 operator*(Vec2 vector, float value)
{
    return vector *= value;
}


////////////////////////////////////////////////////////////////////////
Vec2 operator*(float value, Vec2 vector)
{
    return vector *= value;
}


////////////////////////////////////////////////////////////////////////
Vec2 operator/(Vec2 vector, float value)
{
    return vector /= value;
}


////////////////////////////////////////////////////////////////////////
Vec2& operator*=(Vec2& vector, float value)
{
    vector.x *= value;
    vector.y *= value;
    return vector;
}


////////////////////////////////////////////////////////////////////////
Vec2& operator/=(Vec2& vector, float value)
{
    vector.x /= value;
    vector.y /= value;
    return vector;
}


////////////////////////////////////////////////////////////////////////
inline float dot(Vec2 a, Vec2 b) 
{
    return a.x*b.x + a.y*b.y;
}


////////////////////////////////////////////////////////////////////////
void addToArray(Vec2 vector, std::vector<float>& array)
{
    array.emplace_back(vector.x);
    array.emplace_back(vector.y);
}