////////////////////////////////////////////////////////////////////////
//
// Quat.hpp - Quaternion class to represent 3D rotations.
//
////////////////////////////////////////////////////////////////////////

#ifndef QUAT_HPP_INCLUDED
#define QUAT_HPP_INCLUDED

class Vec3;


class Quat
{
public:

    // Constructor sets up the identity quaternion or a rotation from an axis and angle.
    Quat();
    Quat(const Vec3& axis, float radians);

    // The rotation angle in radians.
    float getAngle() const;

    // The normalized rotation axis.
    Vec3 getAxis() const;

    // Inverse quaternion. Makes the quaternion rotate in the opposite direction.
    Quat inverse() const;

    // Exponentiate quaternion. With exponentiation we can take a fraction of a quaternion.
    // For example a fourth of the quaternion rotation can be retrieved with exponent=0.25.
    Quat exponentiate(float exponent) const;

    // Set the quaternion to the identity (1, (0,0,0)).
    Quat& setupIdentity();

    // Construct the quaternion from an angle and a rotation axis.
    Quat& setupRotation(Vec3 axis, float radians);
    Quat& setupRotationAroundXAxis(float radians);
    Quat& setupRotationAroundYAxis(float radians);
    Quat& setupRotationAroundZAxis(float radians);

    // Four members w,x,y,z. If a rotation phi is around a normalized axis r=(n,m,o)
    // then w = cos(phi/2) and (x,y,z) = sin(phi/2)*r.
    float w;
    float x;
    float y;
    float z;

};

// Multiplication represent the quaternion cross product. The cross product can be used to combine
// rotation quaternions together.
Quat operator*(Quat left, Quat right);
Quat& operator*=(Quat& left, Quat right);

// Quaternion dot product. 
float dot(Quat left, Quat right);

// Spherical Linear Interpolation. Returns a quaternion that has a rotation, which is between
// quaternion a and b's rotation. The resulting rotation is the fraction t from a to b. For
// example with t=0.25 the returned quaternion is a fourth of the way between a and b. The
// parameter t is assumed to be between 0.0 and 1.0.
Quat slerp(Quat a, Quat b, float t);


#endif // QUAT_HPP_INCLUDED
