////////////////////////////////////////////////////////////////////////
//
// Vec4.hpp - A 4-dimensional floating point vector.
//
////////////////////////////////////////////////////////////////////////

#ifndef VEC4_HPP_INCLUDED
#define VEC4_HPP_INCLUDED

#include <vector>
class Vec3;


class Vec4
{
public:

    Vec4();
    Vec4(float x, float y, float z, float w);
    Vec4(Vec3 xyz, float w);

    float length() const;
    float lengthSquare() const;

    Vec4& normalize();

    // Vector addition and substraction.
    Vec4 operator-() const;
    Vec4 operator+(Vec4 vector) const;
    Vec4 operator-(Vec4 vector) const;
    Vec4& operator+=(Vec4 vector);
    Vec4& operator-=(Vec4 vector);

    float x;
    float y;
    float z;
    float w;

};

// Vector multiplication and division.
Vec4 operator*(Vec4 vector, float value);
Vec4 operator*(float value, Vec4 vector);
Vec4 operator/(Vec4 vector, float value);
Vec4& operator*=(Vec4& vector, float value);
Vec4& operator/=(Vec4& vector, float value);

// Dot product of two vectors.
float dot(Vec4 a, Vec4 b);

// Add a vector to the back of an array
void addToArray(Vec4 vector, std::vector<float>& array);


#endif // VEC4_HPP_INCLUDED