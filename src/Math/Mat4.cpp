////////////////////////////////////////////////////////////////////////
//
// Mat4.cpp
//
////////////////////////////////////////////////////////////////////////



#include <cassert>
#include "Mat4.hpp"
#include "Math.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"
#include "Quat.hpp"



////////////////////////////////////////////////////////////////////////
//
// Mat4 public methods
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Mat4::Mat4()
{

}


////////////////////////////////////////////////////////////////////////
Mat4& Mat4::setupIdentity()
{
    m[0] = 1.0f; m[4] = 0.0f; m[8] = 0.0f; m[12] = 0.0f;
    m[1] = 0.0f; m[5] = 1.0f; m[9] = 0.0f; m[13] = 0.0f;
    m[2] = 0.0f; m[6] = 0.0f; m[10] = 1.0f; m[14] = 0.0f;
    m[3] = 0.0f; m[7] = 0.0f; m[11] = 0.0f; m[15] = 1.0f;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat4& Mat4::setupTranslation(Vec3 translation)
{
    setupIdentity();
    m[12] = translation.x;
    m[13] = translation.y;
    m[14] = translation.z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat4& Mat4::setupRotation(Vec3 axis, float angle)
{
    m[3] = m[7] = m[11] = m[12] = m[13] = m[14] = 0.0f;
    m[15] = 1.0f;

    float sin = Math::sin(angle);
    float cos = Math::cos(angle);
    float oneMinusCos = 1.0f - cos;
    axis.normalize();

    // From the standard rotation formula we compute the matrix elements.
    float x = axis.x;
    float y = axis.y;
    float z = axis.z;
    m[0] = x*x*oneMinusCos + cos;
    m[1] = y*x*oneMinusCos + z*sin;
    m[2] = z*x*oneMinusCos - y*sin;

    m[4] = x*y*oneMinusCos - z*sin;
    m[5] = y*y*oneMinusCos + cos;
    m[6] = z*y*oneMinusCos + x*sin;

    m[8] = x*z*oneMinusCos + y*sin;
    m[9] = y*z*oneMinusCos - x*sin;
    m[10] = z*z*oneMinusCos + cos;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat4& Mat4::setupRotation(Quat q)
{
    m[3] = m[7] = m[11] = m[12] = m[13] = m[14] = 0.0f;
    m[15] = 1.0f;

    // We use a derived formula to convert the quaternion to a rotation matrix.
    float xx = 2.0f*q.x*q.x;
    float yy = 2.0f*q.y*q.y;
    float zz = 2.0f*q.z*q.z;

    m[0] = 1.0f - yy - zz;
    m[1] = 2.0f*q.x*q.y + 2.0f*q.w*q.z;
    m[2] = 2.0f*q.x*q.z - 2.0f*q.w*q.y;

    m[4] = 2.0f*q.x*q.y - 2.0f*q.w*q.z;
    m[5] = 1.0f - xx - zz;
    m[6] = 2.0f*q.y*q.z + 2.0f*q.w*q.x;

    m[8] = 2.0f*q.x*q.z + 2.0f*q.w*q.y;
    m[9] = 2.0f*q.y*q.z - 2.0f*q.w*q.x;
    m[10] = 1.0f - xx - yy;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat4& Mat4::setupScale(Vec3 axis, float scale)
{
    m[3] = m[7] = m[11] = m[12] = m[13] = m[14] = 0.0f;
    m[15] = 1.0f;

    axis.normalize();
    float scaleMinusOne = scale - 1.0f;

    float x = axis.x;
    float y = axis.y;
    float z = axis.z;

    m[0] = 1.0f + scaleMinusOne*x*x;
    m[1] = scaleMinusOne*x*y;
    m[2] = scaleMinusOne*x*z;

    m[4] = scaleMinusOne*x*y;
    m[5] = 1.0f + scaleMinusOne*y*y;
    m[6] = scaleMinusOne*y*z;

    m[8] = scaleMinusOne*x*z;
    m[9] = scaleMinusOne*y*z;
    m[10] = 1.0f + scaleMinusOne*z*z;
    return *this;
}


////////////////////////////////////////////////////////////////////////
void Mat4::setColumns(Vec4 first, Vec4 second, Vec4 third, Vec4 fourth)
{
    m[0] = first.x; m[4] = second.x; m[8] = third.x; m[12] = fourth.x;
    m[1] = first.y; m[5] = second.y; m[9] = third.y; m[13] = fourth.y;
    m[2] = first.z; m[6] = second.z; m[10] = third.z; m[14] = fourth.z;
    m[3] = first.w; m[7] = second.w; m[11] = third.w; m[15] = fourth.w;
}


////////////////////////////////////////////////////////////////////////
void Mat4::setRows(Vec4 first, Vec4 second, Vec4 third, Vec4 fourth)
{
    m[0] = first.x; m[4] = first.y; m[8] = first.z; m[12] = first.w;
    m[1] = second.x; m[5] = second.y; m[9] = second.z; m[13] = second.w;
    m[2] = third.x; m[6] = third.y; m[10] = third.z; m[14] = third.w;
    m[3] = fourth.x; m[7] = fourth.y; m[11] = fourth.z; m[15] = fourth.w;
}



////////////////////////////////////////////////////////////////////////
float& Mat4::operator[](int element)
{
    assert(element >= 0 && element < 16);
    return m[element];
}


////////////////////////////////////////////////////////////////////////
const float& Mat4::operator[](int element) const
{
    assert(element >= 0 && element < 16);
    return m[element];
}


////////////////////////////////////////////////////////////////////////
const float* Mat4::toArray() const
{
    return m;
}


////////////////////////////////////////////////////////////////////////
float Mat4::determinant() const
{
    return
        m[0]*( m[5]*(m[10]*m[15]-m[11]*m[14]) - m[9]*(m[6]*m[15]-m[7]*m[14]) + m[13]*(m[6]*m[11]-m[10]*m[7]) ) -
        m[4]*( m[1]*(m[10]*m[15]-m[11]*m[14]) - m[9]*(m[2]*m[15]-m[3]*m[14]) + m[13]*(m[2]*m[11]-m[10]*m[3]) ) +
        m[8]*( m[1]*(m[6]*m[15]-m[7]*m[14]) - m[5]*(m[2]*m[15]-m[3]*m[14]) + m[13]*(m[2]*m[7]-m[3]*m[6]) ) -
        m[12]*( m[1]*(m[6]*m[11]-m[7]*m[10]) - m[5]*(m[2]*m[11]-m[3]*m[10]) + m[9]*(m[2]*m[7]-m[3]*m[6]) );
}


////////////////////////////////////////////////////////////////////////
Mat4 Mat4::inverse() const
{
    float det = determinant();
    if (Math::isZero(det)) {
        assert(false);
        return Mat4().setupIdentity();
    }

    // Calculate the inverse matrix the hard(coded) way.
    Mat4 result;
    result[0] = m[9]*m[14]*m[7] - m[13]*m[10]*m[7] + m[13]*m[6]*m[11] - m[5]*m[14]*m[11] - m[9]*m[6]*m[15] + m[5]*m[10]*m[15];
    result[1] = m[13]*m[10]*m[3] - m[9]*m[14]*m[3] - m[13]*m[2]*m[11] + m[1]*m[14]*m[11] + m[9]*m[2]*m[15] - m[1]*m[10]*m[15];
    result[2] = m[5]*m[14]*m[3] - m[13]*m[6]*m[3] + m[13]*m[2]*m[7] - m[1]*m[14]*m[7] - m[5]*m[2]*m[15] + m[1]*m[6]*m[15];
    result[3] = m[9]*m[6]*m[3] - m[5]*m[10]*m[3] - m[9]*m[2]*m[7] + m[1]*m[10]*m[7] + m[5]*m[2]*m[11] - m[1]*m[6]*m[11];
    result[4] = m[12]*m[10]*m[7] - m[8]*m[14]*m[7] - m[12]*m[6]*m[11] + m[4]*m[14]*m[11] + m[8]*m[6]*m[15] - m[4]*m[10]*m[15];
    result[5] = m[8]*m[14]*m[3] - m[12]*m[10]*m[3] + m[12]*m[2]*m[11] - m[0]*m[14]*m[11] - m[8]*m[2]*m[15] + m[0]*m[10]*m[15];
    result[6] = m[12]*m[6]*m[3] - m[4]*m[14]*m[3] - m[12]*m[2]*m[7] + m[0]*m[14]*m[7] + m[4]*m[2]*m[15] - m[0]*m[6]*m[15];
    result[7] = m[4]*m[10]*m[3] - m[8]*m[6]*m[3] + m[8]*m[2]*m[7] - m[0]*m[10]*m[7] - m[4]*m[2]*m[11] + m[0]*m[6]*m[11];
    result[8] = m[8]*m[13]*m[7] - m[12]*m[9]*m[7] + m[12]*m[5]*m[11] - m[4]*m[13]*m[11] - m[8]*m[5]*m[15] + m[4]*m[9]*m[15];
    result[9] = m[12]*m[9]*m[3] - m[8]*m[13]*m[3] - m[12]*m[1]*m[11] + m[0]*m[13]*m[11] + m[8]*m[1]*m[15] - m[0]*m[9]*m[15];
    result[10] = m[4]*m[13]*m[3] - m[12]*m[5]*m[3] + m[12]*m[1]*m[7] - m[0]*m[13]*m[7] - m[4]*m[1]*m[15] + m[0]*m[5]*m[15];
    result[11] = m[8]*m[5]*m[3] - m[4]*m[9]*m[3] - m[8]*m[1]*m[7] + m[0]*m[9]*m[7] + m[4]*m[1]*m[11] - m[0]*m[5]*m[11];
    result[12] = m[12]*m[9]*m[6] - m[8]*m[13]*m[6] - m[12]*m[5]*m[10] + m[4]*m[13]*m[10] + m[8]*m[5]*m[14] - m[4]*m[9]*m[14];
    result[13] = m[8]*m[13]*m[2] - m[12]*m[9]*m[2] + m[12]*m[1]*m[10] - m[0]*m[13]*m[10] - m[8]*m[1]*m[14] + m[0]*m[9]*m[14];
    result[14] = m[12]*m[5]*m[2] - m[4]*m[13]*m[2] - m[12]*m[1]*m[6] + m[0]*m[13]*m[6] + m[4]*m[1]*m[14] - m[0]*m[5]*m[14];
    result[15] = m[4]*m[9]*m[2] - m[8]*m[5]*m[2] + m[8]*m[1]*m[6] - m[0]*m[9]*m[6] - m[4]*m[1]*m[10] + m[0]*m[5]*m[10];

    result[0] /= det;
    result[1] /= det;
    result[2] /= det;
    result[3] /= det;
    result[4] /= det;
    result[5] /= det;
    result[6] /= det;
    result[7] /= det;
    result[8] /= det;
    result[9] /= det;
    result[10] /= det;
    result[11] /= det;
    result[12] /= det;
    result[13] /= det;
    result[14] /= det;
    result[15] /= det;
    return result;
}


////////////////////////////////////////////////////////////////////////
Mat4 Mat4::transpose() const
{
    Mat4 result;
    result[0] = m[0];  result[4] = m[1]; result[8] = m[2]; result[12] = m[3];
    result[1] = m[4];  result[5] = m[5]; result[9] = m[6]; result[13] = m[7];
    result[2] = m[8];  result[6] = m[9]; result[10] = m[10]; result[14] = m[11];
    result[3] = m[12]; result[7] = m[13]; result[11] = m[14]; result[15] = m[15];
    return result;
}


////////////////////////////////////////////////////////////////////////
//
// Mat4 static methods
//
////////////////////////////////////////////////////////////////////////


Mat4 Mat4::identity()
{
    return Mat4().setupIdentity();
}


Mat4 Mat4::rotate(Vec3 axis, float angle)
{
    return Mat4().setupRotation(axis, angle);
}


Mat4 Mat4::rotateX(float angle)
{
    return Mat4().setupRotation(Vec3(1,0,0), angle);
}


Mat4 Mat4::rotateY(float angle)
{
    return Mat4().setupRotation(Vec3(0,1,0), angle);
}


Mat4 Mat4::rotateZ(float angle)
{
    return Mat4().setupRotation(Vec3(0,0,1), angle);
}


Mat4 Mat4::translate(Vec3 translation)
{
    return Mat4().setupTranslation(translation);
}


Mat4 Mat4::scale(Vec3 axis, float scale)
{
    return Mat4().setupScale(axis, scale);
}


Mat4 Mat4::perspective(float fovY, float aspect, float zNear, float zFar)
{
    Mat4 result;
    result.setupIdentity();

    float top = zNear*Math::tan(Math::degToRad(fovY)/2.0f);
    float right = top*aspect;
    result[0] = zNear/right;
    result[5] = zNear/top;
    result[10] = -(zFar + zNear)/(zFar - zNear);
    result[11] = -1.0f;
    result[14] = -2.0f*zFar*zNear/(zFar - zNear);
    result[15] = 0.0f;

    return result;
}



////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Mat4 operator*(const Mat4& a, const Mat4& b)
{
    Mat4 result;
    result[0] = a[0]*b[0] + a[4]*b[1] + a[8]*b[2] + a[12]*b[3];
    result[1] = a[1]*b[0] + a[5]*b[1] + a[9]*b[2] + a[13]*b[3];
    result[2] = a[2]*b[0] + a[6]*b[1] + a[10]*b[2] + a[14]*b[3];
    result[3] = a[3]*b[0] + a[7]*b[1] + a[11]*b[2] + a[15]*b[3];
    result[4] = a[0]*b[4] + a[4]*b[5] + a[8]*b[6] + a[12]*b[7];
    result[5] = a[1]*b[4] + a[5]*b[5] + a[9]*b[6] + a[13]*b[7];
    result[6] = a[2]*b[4] + a[6]*b[5] + a[10]*b[6] + a[14]*b[7];
    result[7] = a[3]*b[4] + a[7]*b[5] + a[11]*b[6] + a[15]*b[7];
    result[8] = a[0]*b[8] + a[4]*b[9] + a[8]*b[10] + a[12]*b[11];
    result[9] = a[1]*b[8] + a[5]*b[9] + a[9]*b[10] + a[13]*b[11];
    result[10] = a[2]*b[8] + a[6]*b[9] + a[10]*b[10] + a[14]*b[11];
    result[11] = a[3]*b[8] + a[7]*b[9] + a[11]*b[10] + a[15]*b[11];
    result[12]  = a[0]*b[12] + a[4]*b[13] + a[8]*b[14] + a[12]*b[15];
    result[13]  = a[1]*b[12] + a[5]*b[13] + a[9]*b[14] + a[13]*b[15];
    result[14]  = a[2]*b[12] + a[6]*b[13] + a[10]*b[14] + a[14]*b[15];
    result[15]  = a[3]*b[12] + a[7]*b[13] + a[11]*b[14] + a[15]*b[15];
    return result;
}


////////////////////////////////////////////////////////////////////////
Mat4& operator*=(Mat4& left, const Mat4& right)
{
    left = right*left;
    return left;
}


////////////////////////////////////////////////////////////////////////
Vec4 operator*(const Mat4& a, const Vec4& b)
{
    float x = a[0]*b.x + a[4]*b.y + a[8]*b.z + a[12]*b.w;
    float y = a[1]*b.x + a[5]*b.y + a[9]*b.z + a[13]*b.w;
    float z = a[2]*b.x + a[6]*b.y + a[10]*b.z + a[14]*b.w;
    float w = a[3]*b.x + a[7]*b.y + a[11]*b.z + a[15]*b.w;
    return Vec4(x, y, z, w);
}


////////////////////////////////////////////////////////////////////////
Vec4& operator*=(Vec4& left, const Mat4& right)
{
    left = right*left;
    return left;
}
