////////////////////////////////////////////////////////////////////////
//
// Debug.hpp - Debugging utilities for the core classes.
//
////////////////////////////////////////////////////////////////////////

#ifndef MATH_DEBUG_HPP_INCLUDED
#define MATH_DEBUG_HPP_INCLUDED

#include <iosfwd>
class Quat;
class Mat3;
class Mat4;
class Vec2;
class Vec3;
class Vec4;


// Print vectors, matrices and quaternions to ostreams (fx cout).
std::ostream& operator<<(std::ostream& stream, const Vec2& vector);
std::ostream& operator<<(std::ostream& stream, const Vec3& vector);
std::ostream& operator<<(std::ostream& stream, const Vec4& vector);
std::ostream& operator<<(std::ostream& stream, const Quat& q);
std::ostream& operator<<(std::ostream& stream, const Mat3& m);
std::ostream& operator<<(std::ostream& stream, const Mat4& m);


#endif // MATH_DEBUG_HPP_INCLUDED