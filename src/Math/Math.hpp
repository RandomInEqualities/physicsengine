//////////////////////////////////////////////////////////////////////////
//
// Math.hpp - Essential math utilities.
//
//////////////////////////////////////////////////////////////////////////

#ifndef MATH_HPP_INCLUDED
#define MATH_HPP_INCLUDED


namespace Math
{
    const float PI = 3.141592653589793f;
    const float PI2 = 2.0f*Math::PI;
    const float PIOVER2 = Math::PI/2.0f;
    const float PIOVER3 = Math::PI/3.0f;
    const float PIOVER4 = Math::PI/4.0f;

    const float FLOAT_ZERO_TOLERANCE = 1e-10f;

    // Perform equality tests on floating-points. Uses the float zero tolerance constant.
    bool isZero(float x);
    bool isEqual(float x, float y);

    float abs(float x);
    float sqrt(float x);

    // Clamp a value to be between min and max.
    template<typename A, typename B>
    void clamp(A& value, const B& min, const B& max);

    // Round to nearest whole number.
    float round(float x);

    // Compute radians from degrees and the other way.
    float radToDeg(float radian);
    float degToRad(float degree);

    // Trigonometric functions. The accept radians.
    float sin(float radian);
    float cos(float radian);
    float tan(float radian);

    // Inverse trigonometric functions. If x is outside [-1,1] we clamp x into the interval.
    float acos(float x);
    float asin(float x);
    float atan(float x);

    // Compute the arc tangent of y/x using the signs of x and y to determine the correct 
    // quadrant. 
    float atan2(float y, float x);

    // Modulus with floating point numbers.
    float modulus(float numerator, float denominator);

} // namespace Math

#include "Math.inl"


#endif // MATH_HPP_INCLUDED