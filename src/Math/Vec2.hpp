////////////////////////////////////////////////////////////////////////
//
// Vec2.hpp - A 2-dimensional floating point vector.
//
////////////////////////////////////////////////////////////////////////

#ifndef VEC2_HPP_INCLUDED
#define VEC2_HPP_INCLUDED

#include <vector>


class Vec2
{
public:

    Vec2();
    Vec2(float x, float y);

    float length() const;
    float lengthSquare() const;

    Vec2& normalize();

    // Vector addition and substraction.
    Vec2 operator-() const;
    Vec2 operator+(Vec2 vector) const;
    Vec2 operator-(Vec2 vector) const;
    Vec2& operator+=(Vec2 vector);
    Vec2& operator-=(Vec2 vector);

    float x;
    float y;

};

// Vector multiplication and division.
Vec2 operator*(Vec2 vector, float value);
Vec2 operator*(float value, Vec2 vector);
Vec2 operator/(Vec2 vector, float value);
Vec2& operator*=(Vec2& vector, float value);
Vec2& operator/=(Vec2& vector, float value);

// Dot product of two vectors.
float dot(Vec2 a, Vec2 b);

// Add a vector to the back of an array
void addToArray(Vec2 vector, std::vector<float>& array);


#endif // VEC2_HPP_INCLUDED