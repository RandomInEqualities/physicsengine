////////////////////////////////////////////////////////////////////////
//
// Vec3.hpp - A 3-dimensional floating point vector.
//
////////////////////////////////////////////////////////////////////////

#ifndef VEC3_HPP_INCLUDED
#define VEC3_HPP_INCLUDED

#include <vector>


class Vec3
{
public:

    Vec3();
    Vec3(float x, float y, float z);

    float length() const;
    float lengthSquare() const;

    Vec3& normalize();

    // Vector addition and substraction.
    Vec3 operator-() const;
    Vec3 operator+(Vec3 vector) const;
    Vec3 operator-(Vec3 vector) const;
    Vec3& operator+=(Vec3 vector);
    Vec3& operator-=(Vec3 vector);

    float x;
    float y;
    float z;

};

// Vector multiplication and division.
Vec3 operator*(Vec3 vector, float value);
Vec3 operator*(float value, Vec3 vector);
Vec3 operator/(Vec3 vector, float value);
Vec3& operator*=(Vec3& vector, float value);
Vec3& operator/=(Vec3& vector, float value);

// Dot product of two vectors.
float dot(Vec3 a, Vec3 b);

// Cross product of two vectors.
Vec3 cross(Vec3 a, Vec3 b);

// Add a vector to the back of an array
void addToArray(Vec3 vector, std::vector<float>& array);


#endif // VEC3_HPP_INCLUDED