////////////////////////////////////////////////////////////////////////
//
// Mat4.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef MAT4_HPP_INCLUDED
#define MAT4_HPP_INCLUDED

class Vec3;
class Vec4;
class Quat;


/*
    A right handed 4-dimensional matrix class.
*/
class Mat4
{

public:

    // Constructor does not do anything. It leaves the matrix values undefined.
    Mat4();

    // Setup the matrix to perform various transformations. Angles are in radians.
    Mat4& setupIdentity();
    Mat4& setupTranslation(Vec3 translation);
    Mat4& setupRotation(Vec3 axis, float angle);
    Mat4& setupRotation(Quat quaternion);
    Mat4& setupScale(Vec3 axis, float scale);

    // Set the columns and rows in the matrix.
    void setColumns(Vec4 first, Vec4 second, Vec4 third, Vec4 fourth);
    void setRows(Vec4 first, Vec4 second, Vec4 third, Vec4 fourth);

    // Access the 16 matrix elements. They are ordered in the following way:
    //        m[0] m[4] m[8]  m[12]
    //        m[1] m[5] m[9]  m[13]
    //        m[2] m[6] m[10] m[14]
    //        m[3] m[7] m[11] m[15]
    float& operator[](int element);
    const float& operator[](int element) const;

    // Get this matrix as an array in column-major order. They are ordered exatly as
    // the [] operator. Transpose in glUniformMatrix should be GL_FALSE.
    const float* toArray() const;

    // Common matrix operations.
    float determinant() const;
    Mat4 inverse() const;
    Mat4 transpose() const;

public:

    // Common matrix transformations. Angles are in radians.
    static Mat4 identity();
    static Mat4 rotate(Vec3 axis, float angle);
    static Mat4 rotateX(float angle);
    static Mat4 rotateY(float angle);
    static Mat4 rotateZ(float angle);
    static Mat4 translate(Vec3 translatation);
    static Mat4 scale(Vec3 axis, float scale);

    // The opengl rendering pipeline expects coordinates in clip space and prepared for
    // perspective divide with the w coordinate. Below we have matrices that does this.
    // We can set up a perspective projection. Field of view should be in degrees.
    static Mat4 perspective(float fovY, float aspect, float zNear, float zFar);

private:

    float m[16];

};

// Mat4-Mat4 multiplication.
Mat4 operator*(const Mat4& left, const Mat4& right);
Mat4& operator*=(Mat4& left, const Mat4& right);

// Mat4-Vec4 multiplication.
Vec4 operator*(const Mat4& left, const Vec4& right);
Vec4& operator*=(Vec4& left, const Mat4& right);


#endif // MAT4_HPP_INCLUDED


