////////////////////////////////////////////////////////////////////////
//
// Debug.cpp
//
////////////////////////////////////////////////////////////////////////



#include <iostream>
#include <iomanip>

#include "Debug.hpp"
#include "Vec2.hpp"
#include "Vec3.hpp"
#include "Vec4.hpp"
#include "Quat.hpp"
#include "Mat3.hpp"
#include "Mat4.hpp"



////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Quat& q)
{
    stream << q.w << " " << q.x << " " << q.y << " " << q.z;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Mat3& m)
{
    auto flags = stream.flags();
    auto precision = stream.precision();
    stream.precision(3);
    stream.setf(std::ios::fixed);
    stream.setf(std::ios::right);

    using std::setw;
    size_t w = 10;
    stream << "\n"
        << setw(w) << m[0] << setw(w) << m[3] << setw(w) << m[6] << "\n"
        << setw(w) << m[1] << setw(w) << m[4] << setw(w) << m[7] << "\n"
        << setw(w) << m[2] << setw(w) << m[5] << setw(w) << m[8] << "\n";

    stream.precision(precision);
    stream.flags(flags);
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Mat4& m)
{
    auto flags = stream.flags();
    auto precision = stream.precision();
    stream.precision(3);
    stream.setf(std::ios::fixed);
    stream.setf(std::ios::right);

    using std::setw;
    size_t w = 10;
    stream << "\n"
        << setw(w) << m[0] << setw(w) << m[4] << setw(w) << m[8]  << setw(w) << m[12] << "\n"
        << setw(w) << m[1] << setw(w) << m[5] << setw(w) << m[9]  << setw(w) << m[13] << "\n"
        << setw(w) << m[2] << setw(w) << m[6] << setw(w) << m[10]  << setw(w) << m[14] << "\n"
        << setw(w) << m[3] << setw(w) << m[7] << setw(w) << m[11]  << setw(w) << m[15] << "\n";

    stream.precision(precision);
    stream.flags(flags);
    return stream;
}



////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Vec2& vector)
{
    stream << vector.x << " " << vector.y;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Vec3& vector)
{
    stream << vector.x << " " << vector.y << " " << vector.z;
    return stream;
}


////////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const Vec4& vector)
{
    stream << vector.x << " " << vector.y << " " << vector.z << " " << vector.w;
    return stream;
}