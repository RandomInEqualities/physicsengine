////////////////////////////////////////////////////////////////////////
//
// Mat3.cpp
//
////////////////////////////////////////////////////////////////////////



#include <cassert>
#include "Mat3.hpp"
#include "Mat4.hpp"
#include "Vec3.hpp"
#include "Quat.hpp"
#include "Math.hpp"



////////////////////////////////////////////////////////////////////////
//
// Mat3 public methods
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Mat3::Mat3()
{

}


////////////////////////////////////////////////////////////////////////
Mat3& Mat3::setupIdentity()
{
    m[0] = 1.0f; m[3] = 0.0f; m[6] = 0.0f;
    m[1] = 0.0f; m[4] = 1.0f; m[7] = 0.0f;
    m[2] = 0.0f; m[5] = 0.0f; m[8] = 1.0f;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat3& Mat3::setupRotation(Vec3 axis, float angle)
{
    axis.normalize();

    float sin = Math::sin(angle);
    float cos = Math::cos(angle);
    float oneMinusCos = 1.0f-cos;

    // From the standard rotation formula we compute the matrix elements.
    m[0] = axis.x*axis.x*oneMinusCos + cos;
    m[1] = axis.y*axis.x*oneMinusCos + axis.z*sin;
    m[2] = axis.z*axis.x*oneMinusCos - axis.y*sin;

    m[3] = axis.x*axis.y*oneMinusCos - axis.z*sin;
    m[4] = axis.y*axis.y*oneMinusCos + cos;
    m[5] = axis.z*axis.y*oneMinusCos + axis.x*sin;

    m[6] = axis.x*axis.z*oneMinusCos + axis.y*sin;
    m[7] = axis.y*axis.z*oneMinusCos - axis.x*sin;
    m[8] = axis.z*axis.z*oneMinusCos + cos;

    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat3& Mat3::setupRotation(Quat q)
{
    // We use a derived formula to convert the quaternion to a rotation matrix.
    float xx = 2.0f*q.x*q.x;
    float yy = 2.0f*q.y*q.y;
    float zz = 2.0f*q.z*q.z;

    m[0] = 1.0f - yy - zz;
    m[1] = 2.0f*q.x*q.y + 2.0f*q.w*q.z;
    m[2] = 2.0f*q.x*q.z - 2.0f*q.w*q.y;

    m[3] = 2.0f*q.x*q.y - 2.0f*q.w*q.z;
    m[4] = 1.0f - xx - zz;
    m[5] = 2.0f*q.y*q.z + 2.0f*q.w*q.x;

    m[6] = 2.0f*q.x*q.z + 2.0f*q.w*q.y;
    m[7] = 2.0f*q.y*q.z - 2.0f*q.w*q.x;
    m[8] = 1.0f - xx - yy;

    return *this;
}


////////////////////////////////////////////////////////////////////////
Mat3& Mat3::setupScale(Vec3 axis, float scale)
{
    axis.normalize();
    float scaleMinusOne = scale - 1.0f;

    m[0] = 1.0f + scaleMinusOne*axis.x*axis.x;
    m[1] = scaleMinusOne*axis.x*axis.y;
    m[2] = scaleMinusOne*axis.x*axis.z;

    m[3] = scaleMinusOne*axis.x*axis.y;
    m[4] = 1.0f + scaleMinusOne*axis.y*axis.y;
    m[5] = scaleMinusOne*axis.y*axis.z;

    m[6] = scaleMinusOne*axis.x*axis.z;
    m[7] = scaleMinusOne*axis.y*axis.z;
    m[8] = 1.0f + scaleMinusOne*axis.z*axis.z;

    return *this;
}


////////////////////////////////////////////////////////////////////////
void Mat3::setColumns(Vec3 first, Vec3 second, Vec3 third)
{
    m[0] = first.x; m[3] = second.x; m[6] = third.x;
    m[1] = first.y; m[4] = second.y; m[7] = third.y;
    m[2] = first.z; m[5] = second.z; m[8] = third.z;
}


////////////////////////////////////////////////////////////////////////
void Mat3::setRows(Vec3 first, Vec3 second, Vec3 third)
{
    m[0] = first.x; m[3] = first.y; m[6] = first.z;
    m[1] = second.x; m[4] = second.y; m[7] = second.z;
    m[2] = third.x; m[5] = third.y; m[8] = third.z;
}


////////////////////////////////////////////////////////////////////////
float& Mat3::operator[](int element)
{
    assert(element >= 0 && element < 9);
    return m[element];
}


////////////////////////////////////////////////////////////////////////
const float& Mat3::operator[](int element) const
{
    assert(element >= 0 && element < 9);
    return m[element];
}


////////////////////////////////////////////////////////////////////////
Mat4 Mat3::toMat4() const
{
    Mat4 result;
    result[0] = m[0]; result[4] = m[3]; result[8]  = m[6]; result[12] = 0.0f;
    result[1] = m[1]; result[5] = m[4]; result[9]  = m[7]; result[13] = 0.0f;
    result[2] = m[2]; result[6] = m[5]; result[10] = m[8]; result[14] = 0.0f;
    result[3] = 0.0f; result[7] = 0.0f; result[11] = 0.0f; result[15] = 1.0f;
    return result;
}


////////////////////////////////////////////////////////////////////////
const float* Mat3::toArray() const
{
    return m;
}


////////////////////////////////////////////////////////////////////////
float Mat3::determinant() const
{
    return 
        m[0]*(m[4]*m[8]-m[5]*m[7]) - 
        m[3]*(m[1]*m[8]-m[2]*m[7]) + 
        m[6]*(m[1]*m[5]-m[2]*m[4]);
}


////////////////////////////////////////////////////////////////////////
Mat3 Mat3::inverse() const
{
    float det = determinant();
    if (Math::isZero(det)) {
        assert(false);
        return Mat3().setupIdentity();
    }
    float oneOverDeterminant = 1.0f/det;

    Mat3 result;
    result[0] = (m[4]*m[8]-m[7]*m[5])*oneOverDeterminant;
    result[1] = (m[7]*m[2]-m[1]*m[8])*oneOverDeterminant;
    result[2] = (m[1]*m[5]-m[4]*m[2])*oneOverDeterminant;

    result[3] = (m[6]*m[5]-m[3]*m[8])*oneOverDeterminant;
    result[4] = (m[0]*m[8]-m[6]*m[2])*oneOverDeterminant;
    result[5] = (m[3]*m[2]-m[0]*m[5])*oneOverDeterminant;

    result[6] = (m[3]*m[7]-m[6]*m[4])*oneOverDeterminant;
    result[7] = (m[6]*m[1]-m[0]*m[7])*oneOverDeterminant;
    result[8] = (m[0]*m[4]-m[3]*m[1])*oneOverDeterminant;

    return result;
}


////////////////////////////////////////////////////////////////////////
Mat3 Mat3::transpose() const
{
    Mat3 result;
    result[0] = m[0]; result[3] = m[1]; result[6] = m[2];
    result[1] = m[3]; result[4] = m[4]; result[7] = m[5];
    result[2] = m[6]; result[5] = m[7]; result[8] = m[8];
    return result;
}



////////////////////////////////////////////////////////////////////////
//
// Mat3 static methods
//
////////////////////////////////////////////////////////////////////////



Mat3 Mat3::identity()
{
    return Mat3().setupIdentity();
}


Mat3 Mat3::rotate(Vec3 axis, float angle)
{
    return Mat3().setupRotation(axis, angle);
}


Mat3 Mat3::rotateX(float angle)
{
    return Mat3().setupRotation(Vec3(1,0,0), angle);
}


Mat3 Mat3::rotateY(float angle)
{
    return Mat3().setupRotation(Vec3(0,1,0), angle);
}


Mat3 Mat3::rotateZ(float angle)
{
    return Mat3().setupRotation(Vec3(0,0,1), angle);
}


Mat3 Mat3::scale(Vec3 axis, float scale)
{
    return Mat3().setupScale(axis, scale);
}



////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Mat3 operator*(const Mat3& a, const Mat3& b)
{
    Mat3 result;
    result[0] = a[0]*b[0] + a[3]*b[1] + a[6]*b[2];
    result[1] = a[1]*b[0] + a[4]*b[1] + a[7]*b[2];
    result[2] = a[2]*b[0] + a[5]*b[1] + a[8]*b[2];

    result[3] = a[0]*b[3] + a[3]*b[4] + a[6]*b[5];
    result[4] = a[1]*b[3] + a[4]*b[4] + a[7]*b[5];
    result[5] = a[2]*b[3] + a[5]*b[4] + a[8]*b[5];

    result[6] = a[0]*b[6] + a[3]*b[7] + a[6]*b[8];
    result[7] = a[1]*b[6] + a[4]*b[7] + a[7]*b[8];
    result[8] = a[2]*b[6] + a[5]*b[7] + a[8]*b[8];
    return result;
}


////////////////////////////////////////////////////////////////////////
Mat3& operator*=(Mat3& left, const Mat3& right)
{
    left = right*left;
    return left;
}


////////////////////////////////////////////////////////////////////////
Vec3 operator*(const Mat3& a, const Vec3& b)
{
    float x = a[0]*b.x + a[3]*b.y + a[6]*b.z;
    float y = a[1]*b.x + a[4]*b.y + a[7]*b.z;
    float z = a[2]*b.x + a[5]*b.y + a[8]*b.z;
    return Vec3(x, y, z);
}


////////////////////////////////////////////////////////////////////////
Vec3& operator*=(Vec3& left, const Mat3& right)
{
    left = right*left;
    return left;
}



