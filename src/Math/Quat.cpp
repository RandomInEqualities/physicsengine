////////////////////////////////////////////////////////////////////////
//
// Quat.cpp
//
////////////////////////////////////////////////////////////////////////



#include <cassert>
#include "Quat.hpp"
#include "Math.hpp"
#include "Vec3.hpp"



////////////////////////////////////////////////////////////////////////
//
// Quat public methods
//
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
Quat::Quat() :
w(1.0f),
x(0.0f),
y(0.0f),
z(0.0f)
{

}


////////////////////////////////////////////////////////////////////////
Quat::Quat(const Vec3& axis, float radians)
{
    setupRotation(axis, radians);
}


////////////////////////////////////////////////////////////////////////
float Quat::getAngle() const
{
    return 2.0f*Math::acos(w);
}


////////////////////////////////////////////////////////////////////////
Vec3 Quat::getAxis() const
{
    // Find the rotation angle.
    float angle = 2.0f*Math::acos(w);
    float sinAngleOver2 = Math::sin(angle/2.0f);

    // If sinAngleOver2 is zero then we have no rotation. We return an arbitrary rotation axis.
    if (Math::isZero(sinAngleOver2)) {
        return Vec3(1.0f, 0.0f, 0.0f);
    }

    return Vec3(x/sinAngleOver2, y/sinAngleOver2, z/sinAngleOver2);
}


////////////////////////////////////////////////////////////////////////
Quat Quat::inverse() const
{
    Quat result;
    result.w = w;
    result.x = -x;
    result.y = -y;
    result.z = -z;
    return result;
}


////////////////////////////////////////////////////////////////////////
Quat Quat::exponentiate(float exponent) const
{
    // We make sure that w != 1 so we do not have sin(alpha)=0. If we do have sin(alpha)=0 then
    // we just return this quaternion as we have no real angular displacement anyway.
    if (!Math::isEqual(w, 1.0f)) {

        // If the quaternion q is equal to [cos(alpha) sin(alpha)*n], then it holds that
        // q^t = exp(t*log(q)) = [cos(alpha*t) sin(alpha*t)*n].
        Quat result;

        float alpha = Math::acos(w);
        result.w = Math::cos(alpha*exponent);

        float sinAlphaExponentOverSinAlpha = Math::sin(alpha*exponent)/Math::sin(alpha);

        result.x = sinAlphaExponentOverSinAlpha*x;
        result.y = sinAlphaExponentOverSinAlpha*y;
        result.z = sinAlphaExponentOverSinAlpha*z;

        return result;

    }
    else {
        return *this;
    }
}


////////////////////////////////////////////////////////////////////////
Quat& Quat::setupIdentity()
{
    w = 1.0;
    x = y = z = 0.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quat& Quat::setupRotation(Vec3 axis, float radians)
{
    // Make sure n is normalized.
    axis.normalize();

    // Assign the rotation axis and angle.
    float sinAngleOver2 = Math::sin(radians/2.0f);
    x = sinAngleOver2*axis.x;
    y = sinAngleOver2*axis.y;
    z = sinAngleOver2*axis.z;
    w = Math::cos(radians/2.0f);
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quat& Quat::setupRotationAroundXAxis(float radians)
{
    w = Math::cos(radians/2.0f);
    x = Math::sin(radians/2.0f);
    y = 0.0;
    z = 0.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quat& Quat::setupRotationAroundYAxis(float radians)
{
    w = Math::cos(radians/2.0f);
    x = 0.0;
    y = Math::sin(radians/2.0f);
    z = 0.0;
    return *this;
}


////////////////////////////////////////////////////////////////////////
Quat& Quat::setupRotationAroundZAxis(float radians)
{
    w = Math::cos(radians/2.0f);
    x = 0.0;
    y = 0.0;
    z = Math::sin(radians/2.0f);
    return *this;
}


////////////////////////////////////////////////////////////////////////
//
// Functions
//
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
Quat operator*(Quat a, Quat b)
{
    Quat result;
    result.w = a.w*b.w - a.x*b.x - a.y*b.y - a.z*b.z;
    result.x = a.w*b.x + a.x*b.w + a.y*b.z - a.z*b.y;
    result.y = a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z;
    result.z = a.w*b.z + a.z*b.w + a.x*b.y - a.y*b.x;
    return result;
}


////////////////////////////////////////////////////////////////////////
Quat& operator*=(Quat& a, Quat b)
{
    a = b*a;
    return a;
}


////////////////////////////////////////////////////////////////////////
float dot(Quat a, Quat b)
{
    return a.w*b.w + a.x*b.x + a.y*b.y + a.z*b.z;
}


////////////////////////////////////////////////////////////////////////
Quat slerp(Quat a, Quat b, float t)
{
    assert((0.0f <= t) && (t <= 1.0f));

    // We find the angle omega between the two quaternions. For this we use that cos(omega)
    // is DotProduct(a,b). If this is negative we switch sign on a, which ensures consistent
    // results.
    float cosOmega = dot(a,b);
    if (cosOmega < 0.0f) {
        a.x = -a.x;
        a.y = -a.y;
        a.z = -a.z;
        a.w = -a.w;
        cosOmega = -cosOmega;
    }

    // We express the wanted quaternion in the basis of a and b. This gives a nice expression
    // where result = k0*a + k1*b.
    float k0, k1;
    if (cosOmega > 1.0f-Math::FLOAT_ZERO_TOLERANCE) {
        // We use Linear Interpolation here result = a + t*(b-a).
        k0 = 1.0f - t;
        k1 = t;
    }
    else {
        // We use Spherical Linear Interpolation.
        float omega = Math::acos(cosOmega);
        float oneOverSinOmega = 1.0f/Math::sin(omega);
        k0 = oneOverSinOmega*Math::sin(omega*(1.0f - t));
        k1 = oneOverSinOmega*Math::sin(omega*t);
    }

    Quat result;
    result.w = k0*a.w + k1*b.w;
    result.x = k0*a.x + k1*b.x;
    result.y = k0*a.y + k1*b.y;
    result.z = k0*a.z + k1*b.z;
    return result;
}


