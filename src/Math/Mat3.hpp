////////////////////////////////////////////////////////////////////////
//
// Mat3.hpp
//
////////////////////////////////////////////////////////////////////////

#ifndef MAT3_HPP_INCLUDED
#define MAT3_HPP_INCLUDED

class Vec3;
class Quat;
class Mat4;


/*
    A right handed 3-dimensional matrix class.
*/
class Mat3
{

public:

    // Constructor does not do anything. The matrix values are undefined.
    Mat3();

    // Setup the matrix to perform various transformations. Angles are in radians.
    Mat3& setupIdentity();
    Mat3& setupRotation(Vec3 axis, float angle);
    Mat3& setupRotation(Quat quaternion);
    Mat3& setupScale(Vec3 axis, float scale);

    // Set the columns and rows in the matrix.
    void setColumns(Vec3 first, Vec3 second, Vec3 third);
    void setRows(Vec3 first, Vec3 second, Vec3 third);

    // Access the 9 matrix elements. They are ordered in the following way:
    //        m[0] m[3] m[6]
    //        m[1] m[4] m[7]
    //        m[2] m[5] m[8]
    float& operator[](int element);
    const float& operator[](int element) const;

    // Get this matrix as an equivalent 4x4 matrix.
    Mat4 toMat4() const;

    // Get this matrix as an array in column-major order. They are ordered exatly as
    // the [] operator. Transpose in glUniformMatrix should be GL_FALSE.
    const float* toArray() const;

    // Common matrix operations.
    float determinant() const;
    Mat3 inverse() const;
    Mat3 transpose() const;

public:

    // Common matrix transformations. Angles are in radians.
    static Mat3 identity();
    static Mat3 rotate(Vec3 axis, float angle);
    static Mat3 rotateX(float angle);
    static Mat3 rotateY(float angle);
    static Mat3 rotateZ(float angle);
    static Mat3 scale(Vec3 axis, float scale);

private:

    float m[9];

};

// Mat3-Mat3 multiplication.
Mat3 operator*(const Mat3& left, const Mat3& right);
Mat3& operator*=(Mat3& left, const Mat3& right);

// Mat3-Vec3 multiplication.
Vec3 operator*(const Mat3& left, const Vec3& right);
Vec3& operator*=(Vec3& left, const Mat3& right);


#endif // MAT3_HPP_INCLUDED


