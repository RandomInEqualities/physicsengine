#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include "Math/Vec3.hpp"
#include <SFML/System/Time.hpp>
class Mat4;
class Vec4;


class Camera
{
public:
    Camera();

    // Set the camera location.
    void setLocation(Vec3 position, Vec3 lookat);

    // Move the camera.
    void strafeLeft(float deltaTime);
    void strafeRight(float deltaTime);
    void moveForward(float deltaTime);
    void moveBackward(float deltaTime);
    void move(Vec3 direction, float deltaTime);

    // Rotate the camera around the x and y axes.
    void rotate(float deltaX, float deltaY);

    Vec3 getPosition() const;
    Vec3 getUp() const;
    Vec3 getLookDirection() const;
    float getSpeed() const;

    // Get the matrix that transform from world coordinates into camera space.
    Mat4 getViewMatrix() const;

    // Get the horizontal field of view.
    float getFieldOfViewY() const;

    // Get the planes that determine how close and far objects needs to be
    // to be clipped away.
    float getFarZ() const;
    float getNearZ() const;

private:

    Vec3 mPosition;
    Vec3 mUp;
    float mSpeed;
    float mYaw;
    float mPitch;

    float mFieldOfViewY;
    float mFarZ;
    float mNearZ;
};


#endif // CAMERA_HPP_INCLUDED