#ifndef OPENGL_HPP_INCLUDED
#define OPENGL_HPP_INCLUDED

#include <vector>
#include <GL/glew.h>
#include "Geometry.hpp"
#include "Shader.hpp"


#ifdef NDEBUG

    // Do not check for error in release build.
    #define glCheckError() ((void)0)

    // Do not check for initialization in release build.
    #define glCheckInitialization() ((void)0)

#else

    // Check for error in debug build.
    #define glCheckError() checkErrorOpenGL(__FILE__, __LINE__)

    // Check if OpenGL version 3.3 has been initialize and is ready to be used.
    #define glCheckInitialization() checkInitializationOpenGL(__FILE__, __LINE__)

#endif


// Class holding parameters to glDrawArrays.
class DrawArrays
{
public:

    std::vector<GLenum> mode;
    std::vector<GLint> first;
    std::vector<GLsizei> count;
    std::vector<std::size_t> material;

};

// Class holding information about a vertex buffer object (VBO).
class VertexBuffer
{
public:

    VertexBuffer();

    GLuint location;
    GLint size;
    GLenum type;
    GLsizei stride;
    GLvoid* offset;

};

// Class holding information about a vertex array object (VAO).
class VertexArray
{
public:

    VertexArray();

    GLuint location;
    Shader::Type shader;
    DrawArrays drawing;

};

// Class holding information about a model that is loaded into vertex
// buffer objects.
class VertexBufferModel
{
public:

    // Empty buffers.
    VertexBufferModel();

    VertexBuffer vertices;
    VertexBuffer normals;
    VertexBuffer texCoords;
    DrawArrays drawing;

    bool haveVertices() const;
    bool haveNormals() const;
    bool haveTextureCoordinates() const;
    bool haveDrawArrays() const;

};

// Create a vertex buffer model on the GPU from a mesh.
VertexBufferModel createVertexBuffer(const IndexMesh& mesh);

// Delete a vertex buffer model and vertex array from GPU storage.
void deleteVertexBuffer(const VertexBufferModel& vertexBuffer);
void deleteVertexArray(const VertexArray& vertexArray);

// Check if OpenGL has generated any error messages. Throws runtime exception
// if an error is detected.
void checkErrorOpenGL(const char* file, int line);

// Check if OpenGL version 3.3 has been loaded and if its functions are ready 
// to be used. Throws runtime exception on failure.
void checkInitializationOpenGL(const char* file, int line);


#endif // OPENGL_HPP_INCLUDED