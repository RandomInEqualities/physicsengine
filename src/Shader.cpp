
#include <string>
#include <fstream>
#include <vector>
#include <cstddef>
#include <stdexcept>
#include <cassert>
#include <iostream>

#include <GL/glew.h>
#include "Shader.hpp"
#include "OpenGL.hpp"


Shader::Shader() : mShaderProgram(0)
{

}


Shader::~Shader()
{
    if (ready()) {
        glDeleteProgram(mShaderProgram);
    }
}


void Shader::bind()
{
    assert(ready());
    glUseProgram(mShaderProgram);
}


bool Shader::ready() {
    return glIsProgram(mShaderProgram) == GL_TRUE;
}


GLint Shader::findAttributeLocation(GLuint program, const std::string& name)
{
    GLint location = glGetAttribLocation(program, name.c_str());
    if (location == -1) {
        std::cerr << "unable to find attribute " << name << std::endl;
    }
    return location;
}


GLint Shader::findUniformLocation(GLuint program, const std::string& name)
{
    GLint location = glGetUniformLocation(program, name.c_str());
    if (location == -1) {
        std::cerr << "unable to find uniform " << name << std::endl;
    }
    return location;
}


GLuint Shader::loadShaderFromFile(const std::string& vertexFilename, const std::string& fragmentFilename)
{
    std::vector<char> vertexSource = getFileContents(vertexFilename);
    std::vector<char> fragmentSource = getFileContents(fragmentFilename);
    return compile(vertexSource.data(), fragmentSource.data());
}


GLuint Shader::loadShaderFromMemory(const std::string& vertexCode, const std::string& fragmentCode)
{
    return compile(vertexCode.c_str(), fragmentCode.c_str());
}


std::vector<GLchar> Shader::getFileContents(const std::string& filename)
{
    std::ifstream file(filename, std::ios::binary);
    if (!file) {
        throw std::runtime_error("unable to open " + filename);
    }

    // find the file size.
    file.seekg(0, std::ios::end);
    std::streamsize size = file.tellg();

    // read file contents.
    std::vector<GLchar> content;
    if (size > 0) {
        file.seekg(0, std::ios::beg);
        content.resize(static_cast<std::size_t>(size));
        file.read(&content[0], size);
    }

    // append a terminating zero.
    content.push_back('\0');

    return content;
}


GLuint Shader::compile(const GLchar* vertexCode, const GLchar* fragmentCode)
{
    // reference: https://www.opengl.org/wiki/Shader_Compilation

    // vertex shader
    GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexCode, nullptr);
    glCompileShader(vertex);

    GLint successVertex = GL_FALSE;
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &successVertex);
    if (successVertex != GL_TRUE) {
        GLchar log[1024];
        glGetShaderInfoLog(vertex, sizeof(log), nullptr, log);
        glDeleteShader(vertex);
        throw std::runtime_error(log);
    }

    // fragment shader
    GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentCode, nullptr);
    glCompileShader(fragment);

    GLint successFragment = GL_FALSE;
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &successFragment);
    if (successFragment != GL_TRUE) {
        GLchar log[1024];
        glGetShaderInfoLog(fragment, sizeof(log), nullptr, log);
        glDeleteShader(vertex);
        glDeleteShader(fragment);
        throw std::runtime_error(log);
    }

    // link vertex and fragment shader into a program.
    GLuint program = glCreateProgram();
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    GLint successLink = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &successLink);
    if (successLink != GL_TRUE) {
        GLchar log[1024];
        glGetProgramInfoLog(program, sizeof(log), nullptr, log);
        glDeleteProgram(program);
        glDeleteShader(vertex);
        glDeleteShader(fragment);
        throw std::runtime_error(log);
    }

    // clean up shaders 
    glDetachShader(program, vertex);
    glDetachShader(program, fragment);
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    // Check if anything went wrong in the above calls.
    glCheckError();

    return program;
}