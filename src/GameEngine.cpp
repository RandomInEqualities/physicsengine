
#include <cassert>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <random>

#include <GL/glew.h>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "GameEngine.hpp"
#include "Scene.hpp"
#include "Camera.hpp"
#include "PhysicsSphere.hpp"
#include "Entity.hpp"
#include "Math/Vec3.hpp"
#include "Math/Math.hpp"
#include "Math/Mat4.hpp"
#include "OpenGL.hpp"


GameEngine::GameEngine() :
    windowSize(800, 800),
    windowCenter(windowSize.x/2, windowSize.y/2),
    timeWarp(1.0f)
{
    // Initialize window and context.
    sf::VideoMode mode(800, 800, 32);

    windowSize.x = mode.width;
    windowSize.y = mode.height;
    windowCenter.x = mode.width/2;
    windowCenter.y = mode.height/2;

    sf::ContextSettings settings(32, 32, 8, 3, 3);
    window.create(mode, "PhysicsEngine", sf::Style::Default, settings);
    window.setActive();

    // Initialize OpenGL.
    // To use OpenGL 3+ functionality we need to load the OpenGL function pointers at
    // runtime. We use GLEW for this purpose. GLEW requires an OpenGL context to do
    // this, which we should have already created.
    if (glewInit() != GLEW_OK) {
        window.close();
        throw std::runtime_error("unable to initialize GLEW");
    }

    glCheckError();
    glCheckInitialization();

    // Summary of created opengl context.
    std::cout << "OpenGL version      :  " << glGetString(GL_VERSION) << "\n";
    std::cout << "OpenGL renderer     :  " << glGetString(GL_RENDERER) << "\n";
    std::cout << "OpenGL vendor       :  " << glGetString(GL_VENDOR) << "\n";
    std::cout << "OpenGL GLSL version :  " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";

    //  Print controls.
    std::cout << "\n========" << "  Controls  " << "========" << std::endl;
    std::cout << "WASD keys to move around" << std::endl;
    std::cout << "Mouse to look around" << std::endl;
    std::cout << "Q and E to speed time up and down" << std::endl;
    std::cout << "P to pause (use Q to unpause)" << std::endl;
    std::cout << "Press 1, 2, 3, 4 to change scenes.\n" << std::endl;

    // Initialize the scene.
    scene.initialize();

    // Load a default scene.
    std::cout << "Loading Scene ... ";
    setupDefaultScene();
    std::cout << "done\n";

}


GameEngine::~GameEngine()
{

}


void GameEngine::setupDefaultScene()
{
    camera.setLocation(Vec3(10,10,10), Vec3(0,0,20));
    SkyBox skybox;
    skybox.generate(camera.getFarZ());
    scene.setSkybox(skybox);

    Terrain terrain;
    terrain.generate(Vec3(0,0,0), 250, 250, 50.0f, 1);
    scene.setTerrain(terrain);

    PointLight light1;
    light1.position = Vec3(-200, -200, 10);
    light1.diffuse = Vec3(1, 1, 1);
    light1.specular = Vec3(1, 1, 1);
    scene.addPointLight(light1);

    int cubeIndex = scene.addEntityFromFile("models/cube.obj");
    Entity& cube = scene.getEntity(cubeIndex);
    cube.state = Entity::State::Statical;
    cube.position = -light1.position;
    Material cubeMat;
    cubeMat.ambient = light1.diffuse/5;
    cubeMat.diffuse = light1.diffuse;
    cubeMat.specular = light1.specular;
    cubeMat.shininess = 200;
    cube.setMaterial(cubeMat);

    std::uniform_real_distribution<float> rand1(-50, 50);
    std::uniform_real_distribution<float> rand2(5, 20);
    std::uniform_real_distribution<float> rand3(-1, 1);
    std::uniform_real_distribution<float> rand4(0.1f, 0.9f);
    std::uniform_real_distribution<float> rand5(20, 2000);

    int waterId = scene.addEntityFromFile("models/water_box.obj");
    Entity& water = scene.getEntity(waterId);
    water.position = Vec3(0, 0, -50);

    // Add a bunch of spheres.
    for (int i = 0; i < 100; i++) {
        int index = scene.addEntityFromFile("models/sphere_hi-res.obj");
        Entity& sphere = scene.getEntity(index);
        sphere.position = Vec3(rand1(rd), rand1(rd), rand2(rd));
        sphere.velocityLinear = Vec3(rand3(rd), rand3(rd), rand3(rd));

        Material material;
        material.ambient = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.diffuse = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.specular = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.shininess = rand5(rd);
        sphere.setMaterial(material);
    }

    // Setup the physics system.
    sphereCollision.setGravity(Vec3(0,0,-4.82f));
    triangleCollision.setGravity(Vec3(0,0,-4.82f));

}



void GameEngine::setupPhysicsTestScene()
{
    Terrain terrain;
    terrain.generate(Vec3(0,0,-50), 20, 20, 1000.0f, 2);
    scene.setTerrain(terrain);

    PointLight light1;
    light1.position = Vec3(10, 0, 0);
    light1.diffuse = Vec3(1, 0, 0);
    light1.specular = Vec3(1, 0, 0);

    DirectionalLight light2;
    light2.direction = Vec3(1, 0, 1);
    light2.diffuse = Vec3(1, 1, 1);
    light2.specular = Vec3(1, 1, 1);

    scene.addPointLight(light1);
    scene.addDirectionalLight(light2);

    int cube1Index = scene.addEntityFromFile("models/cube.obj");
    Entity& cube1 = scene.getEntity(cube1Index);
    cube1.position = Vec3(00, 10, 10);
    cube1.velocityLinear = Vec3(0, -1, 0);
    cube1.velocityAngular = Vec3(0, 0, 0);
    cube1.rotation = Mat3::rotateX(0.7f) * Mat3::rotateZ(0.7f);

    int cube2Index = scene.addEntityFromFile("models/cube.obj");
    Entity& cube2 = scene.getEntity(cube2Index);
    cube2.position = Vec3(00, -10, 10);
    cube2.velocityLinear = Vec3(0, 1, 0);
    cube2.velocityAngular = Vec3(0, 0, 0);
    cube2.rotation = Mat3::rotateY(0.7f) * Mat3::rotateZ(0.7f);

    int cube3Index = scene.addEntityFromFile("models/cube.obj");
    Entity& cube3 = scene.getEntity(cube3Index);
    cube3.position = Vec3(10, 00, 10);
    cube3.velocityLinear = Vec3(-1, 0, 0);
    cube3.velocityAngular = Vec3(0, 0, 0);
    cube3.rotation = Mat3::rotateX(0.7f) * Mat3::rotateZ(0.7f);

    int cube4Index = scene.addEntityFromFile("models/cube.obj");
    Entity& cube4 = scene.getEntity(cube4Index);
    cube4.position = Vec3(-10, 00, 10);
    cube4.velocityLinear = Vec3(3, 0, 0);
    cube4.velocityAngular = Vec3(0, 0, 0);
    cube4.rotation = Mat3::rotateX(0.7f) * Mat3::rotateZ(0.7f);

}


void GameEngine::setupReleaseScene1()
{
    camera.setLocation(Vec3(10,10,10), Vec3(0,0,20));
    SkyBox skybox;
    skybox.generate(camera.getFarZ());
    scene.setSkybox(skybox);

    Terrain terrain;
    terrain.generate(Vec3(0,0,0), 250, 250, 50.0f, 1);
    scene.setTerrain(terrain);

    PointLight light1;
    light1.position = Vec3(-200, -200, 10);
    light1.diffuse = Vec3(1, 1, 1);
    light1.specular = Vec3(1, 1, 1);
    scene.addPointLight(light1);

    int cubeIndex = scene.addEntityFromFile("models/cube.obj");
    Entity& cube = scene.getEntity(cubeIndex);
    cube.state = Entity::State::Statical;
    cube.position = -light1.position;
    Material cubeMat;
    cubeMat.ambient = light1.diffuse/5;
    cubeMat.diffuse = light1.diffuse;
    cubeMat.specular = light1.specular;
    cubeMat.shininess = 200;
    cube.setMaterial(cubeMat);

    std::uniform_real_distribution<float> rand1(-50, 50);
    std::uniform_real_distribution<float> rand2(0, 10);
    std::uniform_real_distribution<float> rand3(-1, 1);
    std::uniform_real_distribution<float> rand4(0.1f, 0.9f);
    std::uniform_real_distribution<float> rand5(20, 2000);

    int waterId = scene.addEntityFromFile("models/water_box.obj");
    Entity& water = scene.getEntity(waterId);
    water.position = Vec3(0, 0, -50);

    // Add a bunch of spheres.
    for (std::size_t i = 0; i < 100; i++) {
        int index = scene.addEntityFromFile("models/sphere_hi-res.obj");
        Entity& sphere = scene.getEntity(index);
        sphere.position = Vec3(rand1(rd), rand1(rd), rand2(rd));
        sphere.velocityLinear = Vec3(rand3(rd), rand3(rd), rand3(rd));

        Material material;
        material.ambient = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.diffuse = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.specular = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.shininess = rand5(rd);
        sphere.setMaterial(material);
    }

    // Setup the physics system.
    sphereCollision.setGravity(Vec3(0,0,-4.82f));
    triangleCollision.setGravity(Vec3(0,0,-4.82f));

}

void GameEngine::setupReleaseScene2()
{
    camera.setLocation(Vec3(10,10,200), Vec3(1,0,1));
    SkyBox skybox;
    skybox.generate(camera.getFarZ());
    scene.setSkybox(skybox);

    Terrain terrain;
    terrain.generate(Vec3(0,0,0), 250, 250, 10.0f, 2);
    scene.setTerrain(terrain);

    std::uniform_real_distribution<float> rand1(-50, 50);
    std::uniform_real_distribution<float> rand2(100, 200);
    std::uniform_real_distribution<float> rand3(-1, 1);
    std::uniform_real_distribution<float> rand4(0.1f, 0.9f);
    std::uniform_real_distribution<float> rand5(20, 2000);

    int waterId = scene.addEntityFromFile("models/water_box.obj");
    Entity& water = scene.getEntity(waterId);
    water.position = Vec3(0, 0, 0);

    // Add a bunch of spheres.
    for (std::size_t i = 0; i < 50; i++) {
        int index = scene.addEntityFromFile("models/sphere_hi-res.obj");
        Entity& sphere = scene.getEntity(index);
        sphere.position = Vec3(rand1(rd), rand1(rd), rand2(rd));
        sphere.velocityLinear = Vec3(rand3(rd), rand3(rd), rand3(rd));

        Material material;
        material.ambient = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.diffuse = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.specular = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.shininess = rand5(rd);
        sphere.setMaterial(material);
    }

    // Setup the physics system.
    sphereCollision.setGravity(Vec3(0,0,-4.82f));
    triangleCollision.setGravity(Vec3(0,0,-4.82f));

}

void GameEngine::setupReleaseScene3()
{
    camera.setLocation(Vec3(-11,10,-30), Vec3(0,0,-50));
    SkyBox skybox;
    skybox.generate(camera.getFarZ());
    scene.setSkybox(skybox);

    Terrain terrain;
    terrain.generate(Vec3(0,0,0), 250, 250, 10.0f, 3);
    scene.setTerrain(terrain);

    PointLight light1;
    light1.position = Vec3(100, 100, 10);
    light1.diffuse = Vec3(1, 0.2f, 0.2f);
    light1.specular = Vec3(1, 0.2f, 0.2f);
    scene.addPointLight(light1);

    int cubeIndex = scene.addEntityFromFile("models/cube.obj");
    Entity& cube = scene.getEntity(cubeIndex);
    cube.state = Entity::State::Statical;
    cube.position = -light1.position;
    Material cubeMat;
    cubeMat.ambient = light1.diffuse;
    cubeMat.diffuse = light1.diffuse;
    cubeMat.specular = light1.specular;
    cubeMat.shininess = 200;
    cube.setMaterial(cubeMat);

    PointLight light2;
    light2.position = Vec3(-100, -100, 10);
    light2.diffuse = Vec3(0.2f, 0.2f, 0.8f);
    light2.specular = Vec3(0.2f, 0.2f, 0.8f);
    scene.addPointLight(light2);

    int cube2Index = scene.addEntityFromFile("models/cube.obj");
    Entity& cube2 = scene.getEntity(cube2Index);
    cube2.state = Entity::State::Statical;
    cube2.position = -light2.position;
    Material cube2Mat;
    cube2Mat.ambient = light2.diffuse;
    cube2Mat.diffuse = light2.diffuse;
    cube2Mat.specular = light2.specular;
    cube2Mat.shininess = 200;
    cube2.setMaterial(cubeMat);

    std::uniform_real_distribution<float> rand1(-30.0f, 30.0f);
    std::uniform_real_distribution<float> rand2(-35.0f, -30.0f);
    std::uniform_real_distribution<float> rand3(-1.0f, 1.0f);
    std::uniform_real_distribution<float> rand4(0.1f, 0.9f);
    std::uniform_real_distribution<float> rand5(20.0f, 2000.0f);

    // Add a bunch of spheres.
    for (std::size_t i = 0; i < 300; i++) {
        int index = scene.addEntityFromFile("models/sphere_low-res.obj");
        Entity& sphere = scene.getEntity(index);
        sphere.position = Vec3(rand1(rd), rand1(rd), rand2(rd));
        sphere.velocityLinear = Vec3(rand3(rd), rand3(rd), rand3(rd));

        Material material;
        material.ambient = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.diffuse = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.specular = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.shininess = rand5(rd);
        sphere.setMaterial(material);
    }

    // Setup the physics system.
    sphereCollision.setGravity(Vec3(0,0,-4.82f));
    triangleCollision.setGravity(Vec3(0,0,-4.82f));

}

void GameEngine::setupReleaseScene4()
{
    camera.setLocation(Vec3(380,380,50), Vec3(0,0,20));
    SkyBox skybox;
    skybox.generate(camera.getFarZ());
    scene.setSkybox(skybox);

    Terrain terrain;
    terrain.generate(Vec3(0,0,0), 250, 250, 50.0f, 1);
    scene.setTerrain(terrain);

    PointLight light1;
    light1.position = Vec3(-200, -200, 10);
    light1.diffuse = Vec3(1, 1, 1);
    light1.specular = Vec3(1, 1, 1);
    scene.addPointLight(light1);

    int cubeIndex = scene.addEntityFromFile("models/cube.obj");
    Entity& cube = scene.getEntity(cubeIndex);
    cube.state = Entity::State::Statical;
    cube.position = -light1.position;
    Material cubeMat;
    cubeMat.ambient = light1.diffuse/5;
    cubeMat.diffuse = light1.diffuse;
    cubeMat.specular = light1.specular;
    cubeMat.shininess = 200;
    cube.setMaterial(cubeMat);

    std::uniform_real_distribution<float> rand1(100, 150);
    std::uniform_real_distribution<float> rand2(5, 20);
    std::uniform_real_distribution<float> rand3(-1, 1);
    std::uniform_real_distribution<float> rand4(0.35f, 0.9f);
    std::uniform_real_distribution<float> rand5(20, 2000);
    std::uniform_real_distribution<float> rand6(0.05f, 0.25f);

    int waterId = scene.addEntityFromFile("models/water_box.obj");
    Entity& water = scene.getEntity(waterId);
    water.position = Vec3(0, 0, -50);

    // Add a bunch of spheres.
    for (std::size_t i = 0; i < 100; i++) {
        int index = scene.addEntityFromFile("models/sphere_hi-res.obj");
        Entity& sphere = scene.getEntity(index);
        sphere.position = Vec3(rand1(rd), rand1(rd), rand2(rd));
        sphere.velocityLinear = Vec3(rand3(rd), rand3(rd), rand3(rd));

        Material material;
        material.ambient = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.diffuse = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.specular = Vec3(rand4(rd), rand4(rd), rand4(rd));
        material.shininess = rand5(rd);
        sphere.setMaterial(material);
    }

    // Setup the physics system.
    sphereCollision.setGravity(Vec3(0,0,-4.82f));
    triangleCollision.setGravity(Vec3(0,0,-4.82f));

    int index = scene.addEntityFromFile("models/black_pearl.obj");
    Entity& sphere = scene.getEntity(index);

    sphere.state = Entity::State::Statical;
    sphere.position = Vec3(400, 400, -30);
    sphere.velocityLinear = Vec3(0, 0, 0);
    sphere.rotation = Mat3::rotateX(-0.5f*3.14f);

    Material material;
    material.ambient = Vec3(rand4(rd), rand4(rd), rand4(rd));
    material.diffuse = Vec3(rand4(rd), rand4(rd), rand4(rd));
    material.specular = Vec3(rand4(rd), rand4(rd), rand4(rd));
    material.shininess = rand5(rd);

    std::vector<Material> materials;
    for (std::size_t index = 0; index < sphere.getGeometry().materials.size(); index++) {
        Material material;
        float r1 = rand6(rd);
        float r2 = rand4(rd);
        float r3 = rand4(rd);
        material.ambient = Vec3(r1, r1, r1);
        material.diffuse = Vec3(r2, r2, r2);
        material.specular = Vec3(r3, r3, r3);
        material.shininess = rand5(rd);
        materials.push_back(material);
    }

    sphere.setMaterial(materials);
}


void GameEngine::start() 
{
    // Enable the window for rendering.
    window.setSize(windowSize);
    window.setPosition(sf::Vector2i(800,0));
    window.setVerticalSyncEnabled(true);
    window.setMouseCursorVisible(false);

    sf::Time updateInterval = sf::seconds(1.0f/120.0f);
    sf::Time lastFrameTime;
    sf::Clock clock;

    while (true) {
        sf::Time time = clock.getElapsedTime();
        sf::Time delta = time - lastFrameTime;

        // Handle any window input events.
        handleWindowEvents();

        if (!window.isOpen()) {
            break;
        }

        if (delta > updateInterval) {
            lastFrameTime = time;
            moveCamera(delta);
            updatePhysics(timeWarp*delta);
        }

        render(timeWarp*time);

        window.display();
    }
}


void GameEngine::handleWindowEvents() 
{
    sf::Event event;
    while (window.pollEvent(event)) {
        switch (event.type) {
        case sf::Event::Closed:
            window.close();
            break;
        case sf::Event::Resized:
            windowResize(event);
            break;
        case sf::Event::KeyPressed:
            keyPress(event);
            break;
        case sf::Event::KeyReleased:
            keyRelease(event);
            break;
        case sf::Event::MouseMoved:
            mouseMove(event);
            break;
        case sf::Event::MouseButtonPressed:
            mousePress(event);
            break;
        default:
            break;
        }
    }

    // Reset mouse position to the center of the screen.
    sf::Mouse::setPosition(windowCenter, window);
}


void GameEngine::mousePress(const sf::Event& event)
{

}


void GameEngine::mouseMove(const sf::Event& event)
{
    // Rotate the camera.
    float xRotation = (float)(event.mouseMove.x - windowCenter.x)/(float)windowCenter.x;
    float yRotation = (float)(event.mouseMove.y - windowCenter.y)/(float)windowCenter.y;
    camera.rotate(xRotation, yRotation);
}


void GameEngine::keyPress(const sf::Event& event)
{
    if (event.key.code == sf::Keyboard::Escape) {
        window.close();
    }
    if (event.key.code == sf::Keyboard::Q) {
        timeWarp += 0.1f;
    }
    if (event.key.code == sf::Keyboard::E) {
        timeWarp -= 0.1f;
    }
    if (event.key.code == sf::Keyboard::P) {
        timeWarp = (timeWarp < 0.5) ? 1.0f : 0.0f;
    }
    if (event.key.code == sf::Keyboard::P) {
        timeWarp = 0;
    }
    if (event.key.code == sf::Keyboard::Num1) {
        scene = Scene();
        scene.initialize();
        setupReleaseScene1();
    }
    if (event.key.code == sf::Keyboard::Num2) {
        scene = Scene();
        scene.initialize();
        setupReleaseScene2();
    }
    if (event.key.code == sf::Keyboard::Num3) {
        scene = Scene();
        scene.initialize();
        setupReleaseScene3();
    }
    if (event.key.code == sf::Keyboard::Num4) {
        scene = Scene();
        scene.initialize();
        setupReleaseScene4();
    }
}


void GameEngine::keyRelease(const sf::Event& event)
{

}


void GameEngine::windowResize(const sf::Event& event)
{
    windowSize.x = event.size.width;
    windowSize.y = event.size.height;
    windowCenter.x = event.size.width/2;
    windowCenter.y = event.size.height/2;
    glViewport(0, 0, event.size.width, event.size.height);
}


void GameEngine::moveCamera(sf::Time delta)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
        camera.strafeLeft(delta.asSeconds());
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
        camera.strafeRight(delta.asSeconds());
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
        camera.moveForward(delta.asSeconds());
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
        camera.moveBackward(delta.asSeconds());
    }
}


void GameEngine::updatePhysics(sf::Time delta)
{
    float deltaTime = delta.asSeconds();
    sphereCollision.update(deltaTime, scene);
}


void GameEngine::render(sf::Time time)
{
    scene.draw(camera, time.asSeconds());
}
