
#include <vector>
#include <stdexcept>
#include <cstddef>
#include <cassert>
#include <GL/glew.h>

#include "SkyShader.hpp"
#include "OpenGL.hpp"
#include "Lights.hpp"
#include "Math/Mat4.hpp"
#include "Math/Math.hpp"
#include "Math/Vec3.hpp"


SkyShader::SkyShader() : 
    positionAttribute(0),
    normalAttribute(0),
    modelMatrixUniform(0),
    viewMatrixUniform(0),
    projectionMatrixUniform(0),
    timeOfDayUniform(0),
    sunColorUniform(0),
    sunColorHorizonUniform(0),
    sunDirectionUniform(0)
{

}


void SkyShader::compile() 
{
    glCheckInitialization();

    if (glIsProgram(mShaderProgram)) {
        glDeleteProgram(mShaderProgram);
    }

    // Load and compile the shader program.
    mShaderProgram = loadShaderFromFile("Shaders/skyshader.vert", "Shaders/skyshader.frag");
    bind();

    // Find the shader parameters.
    positionAttribute = findAttributeLocation(mShaderProgram, "position");
    //normalAttribute = findAttributeLocation(mShaderProgram, "normal");

    modelMatrixUniform = findUniformLocation(mShaderProgram, "modelMatrix");
    viewMatrixUniform = findUniformLocation(mShaderProgram, "viewMatrix");
    projectionMatrixUniform = findUniformLocation(mShaderProgram, "clipMatrix");
    timeOfDayUniform = findUniformLocation(mShaderProgram, "timeOfDay");
    sunColorUniform = findUniformLocation(mShaderProgram, "sunColor");
    sunColorHorizonUniform = findUniformLocation(mShaderProgram, "sunColorHorizon");
    sunDirectionUniform = findUniformLocation(mShaderProgram, "sunDirection");

    // Check for any errors.
    glCheckError();
}


VertexArray SkyShader::createVertexArray(const VertexBufferModel& model)
{
    assert(ready());
    bind();

    VertexArray vao;
    glGenVertexArrays(1, &vao.location);
    glBindVertexArray(vao.location);

    // Vertex positions.
    if (!model.haveVertices()) {
        throw std::runtime_error("SkyShader: VertexBufferModel does not have vertex data.");
    }

    glBindBuffer(GL_ARRAY_BUFFER, model.vertices.location);
    glVertexAttribPointer(
        positionAttribute, 
        model.vertices.size, 
        model.vertices.type, 
        GL_FALSE, 
        model.vertices.stride, 
        model.vertices.offset
        );
    glEnableVertexAttribArray(positionAttribute);

    glCheckError();

    vao.shader = Shader::Type::Sky;
    vao.drawing = model.drawing;
    return vao;
}


void SkyShader::setProjectionMatrix(const Mat4& matrix)
{
    if (projectionMatrixUniform != -1) {
        glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, matrix.toArray());
    }
}


void SkyShader::setModelMatrix(const Mat4& matrix)
{
    if (modelMatrixUniform != -1) {
        glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, matrix.toArray());
    }
}


void SkyShader::setViewMatrix(const Mat4& matrix)
{
    if (viewMatrixUniform != -1) {
        glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, matrix.toArray());
    }
}


void SkyShader::setTimeOfDay(float time)
{
    if (timeOfDayUniform != -1) {
        glUniform1f(timeOfDayUniform, time);
    }
}


void SkyShader::setSun(const DirectionalLight& light)
{
    Vec3 sunColor = light.diffuse;
    Vec3 sunColorHorizon = light.diffuse/2.0f;
    if (sunColorUniform != -1) {
        glUniform3f(sunColorUniform, sunColor.x, sunColor.y, sunColor.z);
    }
    if (sunColorHorizonUniform != -1) {
        glUniform3f(sunColorHorizonUniform, sunColorHorizon.x, sunColorHorizon.y, sunColorHorizon.z);
    }
    if (sunDirectionUniform != -1) {
        glUniform3f(sunDirectionUniform, light.direction.x, light.direction.y, light.direction.z);
    }
}