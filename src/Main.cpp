
#include <cstdlib>
#include <iostream>

#include "GameEngine.hpp"


int main(int argv, char** argc)
{
    // Create game engine and enter the main loop.
    try 
    {
        GameEngine game;
        game.start();
    }
    catch (const std::exception& error) 
    {
        std::cerr << "Unhandled Exception: " << error.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...) 
    {
        std::cerr << "Unhandled Exception: " << "unknown exception type" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}