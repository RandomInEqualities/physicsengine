#ifndef TERRAIN_HPP_INCLUDED
#define TERRAIN_HPP_INCLUDED

#include <vector>
#include "Geometry.hpp"
#include "Math/Vec3.hpp"
class Mat4;


class Terrain
{
public:

    Terrain();

    const IndexMesh& getGeometry() const;
    float getHeight(float worldX, float worldY) const;
    Vec3 getNormal(float worldX, float worldY) const;
    Mat4 getModelMatrix() const;

    void generate(Vec3 center, int pointsX, int pointsY, float triangleSize, int type);

    // Public for testing add getter when done :).
    float mElasticCoefficient;
    Vec3 mCenter;

private:

    float getTerrainCoordinate(float worldX) const;
    Vec3 getLocalNormal(float worldX, float worldY) const;

    int mPointsX;
    int mPointsY;
    float mTriangleSize;

    IndexMesh mMesh;

};


#endif // TERRAIN_HPP_INCLUDED
