
#include <vector>
#include <iostream>
#include "PhysicsSphere.hpp"
#include "Scene.hpp"
#include "Physics.hpp"
#include "Entity.hpp"
#include "Terrain.hpp"
#include "Math/Math.hpp"
#include "Math/Debug.hpp"


PhysicsSphere::PhysicsSphere() :
    gravity(0,0,0)
{

}


PhysicsSphere::~PhysicsSphere()
{

}


void PhysicsSphere::setGravity(Vec3 gravity) {
    this->gravity = gravity;
}


void PhysicsSphere::update(float deltaTime, Scene& scene) 
{
    if (scene.getEntityAmount() == 0) {
        return;
    }

    std::map<int, Entity>::iterator firstEntityPair = scene.getEntityFirst();
    std::map<int, Entity>::iterator lastEntityPair = scene.getEntityLast();

    // Update each entity.
    for (auto iterator = firstEntityPair; iterator != lastEntityPair; iterator++) {
        Entity& entity = iterator->second;

        if (entity.state != Entity::State::Dynamical) {
            continue;
        }

        // Move entity.
        entity.position += deltaTime * entity.velocityLinear;

        // Rotate entity.
        entity.rotation *= 
            Mat3::rotateX(entity.velocityAngular.x) * 
            Mat3::rotateY(entity.velocityAngular.y) * 
            Mat3::rotateZ(entity.velocityAngular.z);

        // Account for gravity.
        entity.velocityLinear += deltaTime * gravity;
        entity.position += 0.5f * deltaTime * deltaTime * gravity;
    }

    // Collide each entity with terrain.
    if (scene.hasTerrain()) {
        for (auto iterator = firstEntityPair; iterator != lastEntityPair; iterator++) {
            Entity& entity = iterator->second;
            if (entity.state != Entity::State::Dynamical) {
                continue;
            }
            Terrain& terrain = scene.getTerrain();
            if (collision(entity, terrain)) {
                impulseInelastic(entity, terrain);
            }
        }
    }

    // Collide the entities with each other.
    for (auto iteratorA = firstEntityPair; iteratorA != lastEntityPair; iteratorA++) {
        Entity& entityA = iteratorA->second;
        if (entityA.state != Entity::State::Dynamical) {
            continue;
        }
        for (auto iteratorB = firstEntityPair; iteratorB != lastEntityPair; iteratorB++) {
            Entity& entityB = iteratorB->second;
            if (iteratorA->first == iteratorB->first) {
                continue;
            }
            if (entityB.state == Entity::State::Water) {
                if (contains(entityA, entityB)) {
                    waterResponse(entityA);
                }
            }
            else {
                if (collision(entityA, entityB)) {
                    impulseInelastic(entityA, entityB);
                }
            }
        }
    }

}


bool PhysicsSphere::collision(const Entity& entity, Terrain& terrain)
{
    // The spheres collide if the distance between the spheres is smaller than 
    // the sum of their radia.
    float height = terrain.getHeight(entity.position.x, entity.position.y);
    if (Math::abs(entity.position.z - height) < entity.boundingRadius)  {
        return true;
    }
    return false;
}


bool PhysicsSphere::collision(const Entity& entity1, const Entity& entity2)
{
    // The spheres collide if the distance between the spheres is smaller than 
    // the sum of their radia.
    if ((entity1.position - entity2.position).length() < entity1.boundingRadius + entity2.boundingRadius) {
        return true;
    }
    return false;
}

bool PhysicsSphere::contains(const Entity& entity1, const Entity& entity2)
{
    // If an entity A is contained inside entity B, return true
    // (For now the calculation is actually just if A is below B) 
    if (entity1.position.z - entity1.boundingRadius*0.5 < entity2.position.z) {
        return true;
    }
    return false;
}



void PhysicsSphere::impulseInelastic(Entity& entityA, Entity& entityB)
{
    // Push the entities away from each other.
    Vec3 difference = entityB.position - entityA.position;
    Vec3 pushAmount = Vec3(difference).normalize() * (entityB.boundingRadius + entityA.boundingRadius - difference.length())/2;
    entityA.position -= pushAmount;
    entityB.position += pushAmount;

    // Do the collision response if they are moving towards each other.
    if (dot(entityA.velocityLinear, entityB.velocityLinear) < 0) {

        // We only do linear impulse response in this implementation.
        float coefficient = 1.0f + entityA.elasticCoefficient * entityB.elasticCoefficient;
        Vec3 velocityDiff = entityB.velocityLinear - entityA.velocityLinear;
        Vec3 positionDiff = entityB.position - entityA.position;
        Vec3 impulse = coefficient * velocityDiff.length() * positionDiff.normalize() / (1.0f/entityA.mass + 1.0f/entityB.mass);
        entityA.velocityLinear -= impulse/entityA.mass;
        entityB.velocityLinear += impulse/entityB.mass;
    }
}


void PhysicsSphere::impulseInelastic(Entity& entity, Terrain& terrain)
{
    // Do linear impulse response against the terrain.
    Vec3 surfaceNormal = terrain.getNormal(entity.position.x, entity.position.y);
    float hitAngle = dot(surfaceNormal, Vec3(entity.velocityLinear).normalize());
    if (hitAngle < 0) {
        float coefficient = 1.0f + entity.elasticCoefficient * terrain.mElasticCoefficient;
        Vec3 impulse = coefficient * entity.velocityLinear.length() * surfaceNormal / (1.0f/entity.mass);
        entity.velocityLinear += impulse/entity.mass;
    }
}


void PhysicsSphere::waterResponse(Entity& entity)
{
    // Simply push the ball up a little.
    entity.velocityLinear *= 0.95f;
    entity.velocityLinear.z += 0.8f;
}