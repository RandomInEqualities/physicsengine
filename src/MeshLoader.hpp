#ifndef MESH_LOADER_HPP_INCLUDED
#define MESH_LOADER_HPP_INCLUDED

#include <string>
#include <map>
#include "Geometry.hpp"


class MeshLoader
{
public:

    // Load geometry and model data from a obj file (and optional mtl files) into an
    // obj model. Once a model has been loaded it is cached so new calls are fast.
    // Throws runtime error if something goes wrong.
    const IndexMesh& loadFromFile(const std::string& filename);

private:

    std::map<std::string, IndexMesh> cache;

};


#endif // MESH_LOADER_HPP_INCLUDED