
#include "Lights.hpp"
#include "Math/Vec3.hpp"


PointLight::PointLight() : 
    position(0,0,0), diffuse(1,1,1), specular(1,1,1)
{

}


PointLight::PointLight(Vec3 POSITION, Vec3 DIFFUSE, Vec3 SPECULAR) :
    position(POSITION), diffuse(DIFFUSE), specular(SPECULAR)
{

}


DirectionalLight::DirectionalLight() : 
    direction(1,0,0), diffuse(1,1,1), specular(1,1,1)
{

}


DirectionalLight::DirectionalLight(Vec3 DIRECTION, Vec3 DIFFUSE, Vec3 SPECULAR) :
    direction(DIRECTION), diffuse(DIFFUSE), specular(SPECULAR)
{

}