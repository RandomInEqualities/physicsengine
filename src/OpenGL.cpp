
#include <string>
#include <stdexcept>
#include <cassert>

#include <GL/glew.h>
#include "OpenGL.hpp"
#include "Shader.hpp"


VertexBuffer::VertexBuffer() : 
    location(0), 
    size(0), 
    type(0), 
    stride(0), 
    offset(0) 
{

}


VertexArray::VertexArray() : 
    location(0), 
    shader(Shader::Type::None) 
{

}


VertexBufferModel::VertexBufferModel()
{

}


bool VertexBufferModel::haveVertices() const
{
    return vertices.location != 0;
}


bool VertexBufferModel::haveNormals() const
{
    return normals.location != 0;
}


bool VertexBufferModel::haveTextureCoordinates() const
{
    return texCoords.location != 0;
}


bool VertexBufferModel::haveDrawArrays() const
{
    return drawing.mode.size() != 0;
}


VertexBufferModel createVertexBuffer(const IndexMesh& mesh)
{
    assert(!mesh.materials.empty());
    assert(!mesh.vertices.empty());
    assert(!mesh.haveOutOfRangeIndices());

    // Create arrays for vertices, normals and texture coordinates (if any).
    VertexBufferModel model;
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;

    // Order the drawing according to materials.
    GLenum mode = GL_TRIANGLES;
    GLint first = 0;
    GLsizei count = 0;
    for (std::size_t materialIndex = 0; materialIndex < mesh.materials.size(); materialIndex++) {

        for (const IndexTriangle& tri : mesh.triangles) {

            // Skip triangle if it does not have the current material.
            if (tri.material != materialIndex) {
                continue;
            }

            // Add triangle to vertex, normal and texture coordinate arrays.
            addToArray(mesh.vertices[tri.v0], vertices);
            addToArray(mesh.vertices[tri.v1], vertices);
            addToArray(mesh.vertices[tri.v2], vertices);
            if (mesh.normals.size() > 0) {
                addToArray(mesh.normals[tri.v0normal], normals);
                addToArray(mesh.normals[tri.v1normal], normals);
                addToArray(mesh.normals[tri.v2normal], normals);
            }
            if (mesh.texCoords.size() > 0) {
                addToArray(mesh.texCoords[tri.v0texCoord], texCoords);
                addToArray(mesh.texCoords[tri.v1texCoord], texCoords);
                addToArray(mesh.texCoords[tri.v2texCoord], texCoords);
            }

            count += 3;
        }

        // Specify how to draw the traingles with this material.
        if (count > 0) {
            model.drawing.mode.emplace_back(mode);
            model.drawing.first.emplace_back(first);
            model.drawing.count.emplace_back(count);
            model.drawing.material.emplace_back(materialIndex);
            first += count;
            count = 0;
        }

    }

    // Create vertex buffer.
    model.vertices.size = 3;
    model.vertices.type = GL_FLOAT;
    glGenBuffers(1, &model.vertices.location);
    glBindBuffer(GL_ARRAY_BUFFER, model.vertices.location);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

    // Create normal buffer.
    if (normals.size() == vertices.size()) {
        model.normals.size = 3;
        model.normals.type = GL_FLOAT;
        glGenBuffers(1, &model.normals.location);
        glBindBuffer(GL_ARRAY_BUFFER, model.normals.location);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * normals.size(), normals.data(), GL_STATIC_DRAW);
    }

    // Create texture coordinate buffer.
    if (texCoords.size() == 2*vertices.size()/3) {
        model.texCoords.size = 2;
        model.texCoords.type = GL_FLOAT;
        glGenBuffers(1, &model.texCoords.location);
        glBindBuffer(GL_ARRAY_BUFFER, model.texCoords.location);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float) * texCoords.size(), texCoords.data(), GL_STATIC_DRAW);
    }

    return model;
}


void deleteVertexBuffer(const VertexBufferModel& vertexBuffer)
{
    GLuint buffers[3] = {
        vertexBuffer.vertices.location, 
        vertexBuffer.normals.location, 
        vertexBuffer.texCoords.location
    };
    glDeleteBuffers(3, buffers);
}


void deleteVertexArray(const VertexArray& vertexArray)
{
    glDeleteVertexArrays(1, &vertexArray.location);
}


void throwRuntimeError(const char* file, int line, const char* message)
{
    throw std::runtime_error(
        std::string(file) + " at line " + std::to_string(line) + ", " + message + "\n"
    );
}


void checkErrorOpenGL(const char* file, int line)
{
    GLenum error = glGetError();
    if (error == GL_NO_ERROR) {
        return;
    }
    const char* message = "";
    switch (error) {
    case GL_INVALID_ENUM:
        message = "GL_INVALID_ENUM, an unacceptable value is specified for an enumerated argument.";
        break;
    case GL_INVALID_VALUE:
        message = "GL_INVALID_VALUE, a numeric argument is out of range.";
        break;
    case GL_INVALID_OPERATION:
        message = "GL_INVALID_OPERATION, the specified operation is not allowed in the current state.";
        break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        message = "GL_INVALID_FRAMEBUFFER_OPERATION, the framebuffer object is not complete.";
        break;
    case GL_OUT_OF_MEMORY:
        message = "GL_OUT_OF_MEMORY, there is not enough memory left to execute the command.";
        break;
    default:
        message = "Unknown Error. Add a new error message to gl::checkError!";
        break;
    }
    throwRuntimeError(file, line, message);
}


void checkInitializationOpenGL(const char* file, int line)
{
    if (!GLEW_VERSION_3_3) {
        throwRuntimeError(file, line, "OpenGL version 3.3 has not been initialized.");
    }
}
