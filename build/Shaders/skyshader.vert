#version 150

in vec3 position;
in vec3 normal;

// Matrix that transform from local object space to world space.
uniform mat4 modelMatrix;

// Matrix that transforms from world space to camera space.
uniform mat4 viewMatrix;

// Matrix that transform from camera space to clip and prepares
// vectors for a perspective divide.
uniform mat4 clipMatrix;

out vec4 worldPosition;
out vec4 worldNormal;

void main(void)
{
	worldPosition = modelMatrix * vec4(position, 1.0);
    worldNormal = modelMatrix * vec4(normal, 0.0);
	gl_Position = clipMatrix * viewMatrix * worldPosition;
}