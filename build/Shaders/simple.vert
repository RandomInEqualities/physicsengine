#version 150

uniform mat4 projection_view;
uniform mat4 model;

in vec3 position;

void main(void)
{
	gl_Position = projection_view * model * vec4(position, 1.0);
}