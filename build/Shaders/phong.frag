#version 150 

in vec4 worldPosition;
in vec4 worldNormal;

uniform mat4 viewMatrix;
uniform mat4 clipMatrix;

const int MAX_NUMBER_OF_LIGHTS = 5;
uniform int numberOfLights;
uniform vec4 lightPosition[MAX_NUMBER_OF_LIGHTS];
uniform vec3 lightDiffuse[MAX_NUMBER_OF_LIGHTS];
uniform vec3 lightSpecular[MAX_NUMBER_OF_LIGHTS];

uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float materialShininess;

out vec4 fragColor;


void main() 
{ 
    vec3 ambient = materialAmbient;
    vec3 diffuse = vec3(0.0f);
    vec3 specular = vec3(0.0f);

    // We calculate the diffuse and specular contributions in eye space. The light
    // comes from lightPosition (in world space) and hits the surface point position.
	vec3 position = (viewMatrix * worldPosition).xyz;
	vec3 normal = (viewMatrix * worldNormal).xyz;
    vec3 viewDir = normalize(-position);
    vec3 normalDir = normalize(normal);

    // Loop over each light source.
    for (int index = 0; index < numberOfLights; index++) {

        // Find the incomming light direction.
        vec4 lightPos = viewMatrix * lightPosition[index];
        vec3 lightDir;
		float distanceToLight = 1;
        if (lightPos.w < 0.5) {
            // Directional light.
            lightDir = -lightPos.xyz;
        }
        else {
            // Point light.
			distanceToLight = 0.05*length(lightPos.xyz - position);
            lightDir = normalize(lightPos.xyz - position);
        }

        // The diffuse contribution.
        float lightDotNormal = dot(lightDir, normalDir);
        if (lightDotNormal > 0.0f) {
            diffuse += lightDotNormal * lightDiffuse[index] * materialDiffuse/distanceToLight;
        }

        // The specular contribution.
        vec3 reflectDir = normalize(2.0f * lightDotNormal * normalDir - lightDir);
        float reflectDotView = dot(reflectDir, viewDir);
        if (reflectDotView > 0.0f) {
            specular += lightSpecular[index] * materialSpecular * pow(reflectDotView, materialShininess)/distanceToLight;
        }

    }

    // Color is the sum of ambient, diffuse and specular.
    fragColor = vec4(ambient + diffuse + specular, 1.0);
} 