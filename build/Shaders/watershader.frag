#version 150
     
in vec4 worldPosition;
in vec4 worldNormal;
     
uniform mat4 viewMatrix;
uniform mat4 clipMatrix;
     
const int MAX_NUMBER_OF_LIGHTS = 5;
uniform float time;
uniform float timeOfDay;
uniform int numberOfLights;
uniform vec4 lightPosition[MAX_NUMBER_OF_LIGHTS];
uniform vec3 lightDiffuse[MAX_NUMBER_OF_LIGHTS];
uniform vec3 lightSpecular[MAX_NUMBER_OF_LIGHTS];
     
uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float materialShininess;
     
out vec4 fragColor;
    
// Get the amount that we should modify the geometric normal vector with.
vec3 getWaterVector(vec2 location) {          
	float s = location.x*0.05f;
	float t = location.y*0.05f;
	float wobble = time;
     
	// Three sine functions in x and y with different periods and amplitudes gives us a crude illusion of moving water.
	return normalize(vec3(
		0.21*sin(s+wobble*0.2)*sin(t*6+wobble*0.12)+0.06*sin(s*5.3-wobble*6)*sin(t*2+wobble*2)-0.08*sin(s/1.5+wobble*2)*sin(t*0.3+wobble*2),
		0.08*sin(t+wobble*6)*sin(t*0.8+wobble*2)+0.12*sin(t*3+wobble*2)*0.07*sin(t*6+wobble*2)-0.05*sin(t/1.5-wobble*4)*sin(t+wobble*2),
		1
	));
}
     
void main()
{
    vec3 ambient = materialAmbient;
    vec3 diffuse = vec3(0.0f);
    vec3 specular = vec3(0.0f);
    vec3 skyColour = vec3(1,50,150);
    vec3 sunColorHorizon = vec3(211,142,33);
    vec3 sunColor = vec3(211,211,211);
     
    // Sky Color changes with the time of day.
    float timeScalar = 0.0;
    if (3 < timeOfDay && timeOfDay < 21) {
            timeScalar = (9.0 - abs(timeOfDay - 12.0))/9.0;
    }
     
    vec3 horizonColour = ((pow(timeScalar,0.2))*vec3(117,186,255) + (1-pow(timeScalar,0.2))*vec3(2255,1222,422));
    ambient = (0.0002*skyColour + 0.003*(1.0/255.0+0.2 + 0.0001*materialAmbient)*horizonColour)*(timeScalar);
    vec3 materialDiffuseSum = 0.00001*vec3(timeScalar*255*sunColor + (1.0/255.0-timeScalar)*255*sunColorHorizon) + materialDiffuse*0.001;
    vec3 materialSpecularSum = .0002*vec3(timeScalar*255*sunColor + 0.001*(1.0/255.0-timeScalar)*255*sunColorHorizon)+ materialSpecular*0.001;
	float shineSum = materialShininess*timeScalar*timeScalar*timeScalar*timeScalar;
    
    // We calculate the diffuse and specular contributions in eye space. The light
    // comes from lightPosition (in world space) and hits the surface point position.
    vec3 position = (viewMatrix * worldPosition).xyz;
    vec3 normal = (viewMatrix * worldNormal).xyz;
    vec3 viewDir = normalize(-position);
    vec3 normalDir = normalize(normal + getWaterVector(worldPosition.xy));
     
    // Loop over each light source.
    for (int index = 0; index < numberOfLights; index++) {
     
        // Find the incomming light direction.
        vec4 lightPos = viewMatrix * lightPosition[index];
		float distanceToLight = 1;
		vec3 diffuseToUse;
		vec3 specularToUse;
        vec3 lightDir;
		float shineToUse;
        if (lightPos.w < 0.5) {
            // Directional light.
			diffuseToUse = materialDiffuseSum;
			specularToUse = materialSpecularSum;
			shineToUse = shineSum;
            lightDir = -normalize(lightPos.xyz);
        }
        else {
            // Point light.
			diffuseToUse = materialDiffuse;
			specularToUse = materialSpecular;
			shineToUse = materialShininess;
			distanceToLight = 0.05*length(lightPos.xyz - position);
            lightDir = normalize(lightPos.xyz - position);
        }
     
        // The diffuse contribution.
        float lightDotNormal = dot(lightDir, normalDir);
        if (lightDotNormal > 0.0f) {
            diffuse += lightDotNormal * lightDiffuse[index] * diffuseToUse/distanceToLight;
        }
     
        // The specular contribution.
        vec3 reflectDir = normalize(2.0f * lightDotNormal * normalDir - lightDir);
        float reflectDotView = dot(reflectDir, viewDir);
        if (reflectDotView > 0.0f) {
            specular += lightSpecular[index] * specularToUse * pow(reflectDotView, shineToUse)/distanceToLight;
        }
     
    }
     
    // Color is the sum of ambient, diffuse and specular.
    fragColor = vec4(ambient + diffuse + specular, 0.7);
}

