#version 150

in vec4 worldPosition;
in vec4 worldNormal;

uniform mat4 viewMatrix;
uniform mat4 clipMatrix;

uniform float timeOfDay;
uniform vec3 sunColor;
uniform vec3 sunColorHorizon;
uniform vec3 sunDirection;

out vec4 fragColor;

void main(void) {

	// If angle between drawed pixel and sun direction is small, we draw the sun.
	vec3 normalizedPosition = normalize(worldPosition.xyz);
	vec3 up = vec3(0.0,0.0,1.0);
	float angleScalar = -dot(normalizedPosition,up)/255;
	float sunAngle = dot(normalizedPosition,normalize(-sunDirection));
	if (sunAngle > 0.997f) {
		// Interpolate between sun color at midday and sun color at dawn.
		fragColor = vec4(angleScalar*255*sunColor + (1.0/255.0-angleScalar)*255*sunColorHorizon, 1.0);
		return;
	}
	
	// Compute the colour of the sky.
	vec4 skyColour = vec4(1,50,150,255);

	// Lower on the horizon more and more particles diffract adding up to a whiter blue.
	if (angleScalar < 0) {
		angleScalar = 0;
	}

	// Sky Color changes with the time of day.
	float timeScalar = 0.0;
	if (3 < timeOfDay && timeOfDay < 21) {
		timeScalar = (9.0 - abs(timeOfDay - 12.0))/9.0;
	}

	// Horizon colour is interpolated between whiteblue at midday and orangey at dawn/dusk.
	vec4 horizonColour = ((pow(timeScalar,0.2))*vec4(117,186,255,255) + (1-pow(timeScalar,0.2))*vec4(2255,1222,422,255));

	// Interpolate between skycolor and horizon color and multiply with the time of day scale.
	fragColor = (angleScalar*skyColour + (1.0/255.0-angleScalar)*horizonColour)*(timeScalar);

}
