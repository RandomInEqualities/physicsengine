
Physics Engine
=========================================

Physics Engine is a small game engine in C++ and OpenGL. It was written for a university course about computer graphics.

Authors
-------

Christian Andersen (csandersen3@gmail.com)
Matias Stachowski Winther (stachowski@gmail.com)

Download
--------

You can get the latest source code from bitbucket (https://bitbucket.org/RandomInEqualities/physicsengine).

Install
-------

1) Install Visual Studio 2012.
2) Open the solution.
3) Go into PhysicsEngine - Right click and select Properties - Configuration Properties - Debugging and set the Working Directory to $(OutDir)
   Set the Working Directory to $(OutDir) for all platforms/configurations that you want to compile for.
3) Run it!

Help
----------

Please let us know if something is wrong!